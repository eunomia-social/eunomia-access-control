/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const OrbitDB = require('orbit-db')
const Keystore = require('orbit-db-keystore')
const DBInterfACE = require('../src/db-interface')
const Identities = require('orbit-db-identity-provider')

const loki = { autosave: false }

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const dbPath2 = './orbitdb/tests/multiple-databases/2'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'
const ipfsPath2 = './orbitdb/tests/multiple-databases/2/ipfs'

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const waitForReplication = async (log, stopSize, time = 250) => {
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      if (log.length >= stopSize) {
        clearInterval(interval)
        resolve()
      }
    }, time)
  })
}

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db-docstore - Object-level Access Control (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfs1, ipfs2
    let orbitdb1, orbitdb2

    let localDatabase, remoteDatabase

    let numberOfEntries

    let entity1, entity2
    let insertObj, entityID1, entityID2

    // Create two IPFS instances and two OrbitDB instances (2 nodes/peers)
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(dbPath1)
      rmrf.sync(dbPath2)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)

      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })
    })

    after(async () => {
      if(orbitdb1)
        await orbitdb1.stop()

      if(orbitdb2)
        await orbitdb2.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)

      if (ipfsd2)
        await stopIpfs(ipfsd2)
    })

    describe('normal behavior', function () {
      before(async () => {
        // This creates the local database
        const options = { create: true, accessController: { write: ['*'] } }
        localDatabase = await orbitdb1.docs('address', options)

        // Open the database on the 2nd node (node2)
        const options2 = { sync: true }
        const address = localDatabase.address.toString()
        remoteDatabase = await orbitdb2.docs(address, options2)

        // Create first entity connected to local database
        entityID1 = 'entityID1'
        entity1 = await DBInterfACE.createDBInterface(entityID1, ipfs1, localDatabase, localDatabase.identity, {loki})

        // Create second entity connected to remote database
        entityID2 = 'entityID2'
        entity2 = await DBInterfACE.createDBInterface(entityID2, ipfs2, remoteDatabase, remoteDatabase.identity, {loki})

        // Add public keys to each access controller
        localDatabase.addKey(entityID2, entity2.publicKey)
        remoteDatabase.addKey(entityID1, entity1.publicKey)
      })

      after(async () => {
        if(localDatabase) await localDatabase.drop()
        if(remoteDatabase) await remoteDatabase.drop()
      })

      it('e1 creates object - e2 can write', async function () {
        // The owner does not need to be included in the permission list
        // Has all permissions by default
        const writePermissions = [ entityID2 ]
        const deletePermissions = []

        insertObj = {
          _id: 'obj1',
          data: 'e1 creates object', // actual object data
          permissions: { // list of permissions
            write: writePermissions, // list of users with write permission
            delete: deletePermissions // list of users with delete permission
          }
        }

        const logSize = remoteDatabase._oplog.length
        await entity1.create(insertObj)
        await waitForReplication(remoteDatabase._oplog, logSize+1)

        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 creates object')

        retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 creates object')
      })

      it('e1 updates object', async function () {
        const newObj = {
          _id: 'obj1',
          data: 'e1 updates object',
          encrypted: false
        }

        const logSize = remoteDatabase._oplog.length
        await entity1.update(newObj)
        await waitForReplication(remoteDatabase._oplog, logSize+1)

        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 updates object')

        retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 updates object')
      })

      it('e2 tries to create object with same ID (owner ambiguity)', async function () {
        const newWritePermissions = []
        const newDeletePermissions = []

        const newObj = {
          _id: 'obj1', // existing object
          data: 'e2 creates object',
          permissions: {
            write: newWritePermissions,
            delete: newDeletePermissions
          }
        }

        const logSize = localDatabase._oplog.length
        await entity2.create(newObj)
        await waitForReplication(localDatabase._oplog, logSize+1)

        // State remains unchanged
        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 updates object')

        retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 updates object')
      })

      it('e2 updates object', async function () {
        const newObj = {
          _id: 'obj1',
          data: 'e2 updates object',
          encrypted: false
        }

        const logSize = localDatabase._oplog.length
        await entity2.update(newObj)
        await waitForReplication(localDatabase._oplog, logSize+1)

        let retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e2 updates object')

        retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e2 updates object')
      })

      it('e2 fails to delete object', async function () {
        const logSize = localDatabase._oplog.length
        await entity2.del(insertObj._id)
        await waitForReplication(localDatabase._oplog, logSize+1)

        let retrievedObjs = await entity2.get('obj1')
        assert.equal(retrievedObjs.length, 1)
        let retrievedObj = retrievedObjs[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e2 updates object')

        retrievedObjs = await entity1.get('obj1')
        assert.equal(retrievedObjs.length, 1)
        retrievedObj = retrievedObjs[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e2 updates object')
      })

      it('e1 changes permissions - removes e2 write permission', async function () {
        const newWritePermissions = []
        const newDeletePermissions = []

        const newObj = {
          _id: 'obj1',
          data: 'e1 changes permissions',
          permissions: {
            write: newWritePermissions,
            delete: newDeletePermissions
          },
          encrypted: false
        }

        const logSize = remoteDatabase._oplog.length
        await entity1.update(newObj)
        await waitForReplication(remoteDatabase._oplog, logSize+1)

        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 changes permissions')

        retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 changes permissions')
      })

      it('e2 fails to update object', async function () {
        const newObj = {
          _id: 'obj1',
          data: 'e2 updates object again',
          encrypted: false
        }

        const logSize = localDatabase._oplog.length
        await entity2.update(newObj)
        await waitForReplication(localDatabase._oplog, logSize+1)

        let retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 changes permissions')

        retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 changes permissions')
      })

      it('e1 deletes object', async function () {
        const logSize = remoteDatabase._oplog.length
        await entity1.del(insertObj._id)
        await waitForReplication(remoteDatabase._oplog, logSize+1)

        let retrievedObjs = await entity1.get('obj1')
        assert.equal(retrievedObjs.length, 0)

        retrievedObjs = await entity2.get('obj1')
        assert.equal(retrievedObjs.length, 0)
      })
    })

    describe('new node processing operations during replication', function () {
      before(async () => {
        // Create new localDatabase instance
        const options = { create: true, accessController: { write: ['*'] } }
        localDatabase = await orbitdb1.docs('address2', options)

        // Add public keys to local access controller
        localDatabase.addKey(entityID1, entity1.publicKey)
        localDatabase.addKey(entityID2, entity2.publicKey)

        // Change entities' database to new localDatabase (both entities use same DB)
        entity1.db = localDatabase
        entity2.db = localDatabase
      })

      after(async () => {
        if(localDatabase) await localDatabase.drop()
        if(remoteDatabase) await remoteDatabase.drop()
      })

      it('e1 creates object - e1 and e2 can write, only e1 can delete', async function () {
        const writePermissions = [ entityID1, entityID2 ]
        const deletePermissions = [ entityID1 ]

        insertObj = {
          _id: 'obj1',
          data: 'e1 creates object', // actual object data
          permissions: { // list of permissions
            write: writePermissions, // list of users with write permission
            delete: deletePermissions // list of users with delete permission
          }
        }

        await entity1.create(insertObj)

        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 creates object')
      })

      it('e2 updates object', async function () {
        const newObj = {
          _id: 'obj1',
          data: 'e2 updates object',
          encrypted: false
        }

        await entity2.update(newObj)

        let retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e2 updates object')
      })

      it('node2 joins the network', async function () {
        // Open the database on the 2nd node (node2)
        const options2 = { sync: true }
        const address = localDatabase.address.toString()
        remoteDatabase = await orbitdb2.docs(address, options2)

        // Add public keys to remote access controller
        remoteDatabase.addKey(entityID1, entity1.publicKey)
        remoteDatabase.addKey(entityID2, entity2.publicKey)
      })

      it('waits for replication', async function () {
        let numberOfEntries = 2
        await waitForReplication(remoteDatabase._oplog, numberOfEntries)

        let retrievedObj = (await remoteDatabase.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data.data, 'e2 updates object')
      })
    })

    describe('owner ambiguity + verifyObjectOwner', () => {
      let entityID3, entity3

      before(async () => {
        // This creates the local database
        const options = { create: true, accessController: { write: ['*'] } }
        localDatabase = await orbitdb1.docs('address3', options)

        const idGen = async (id) => {
          return await Identities.createIdentity({ id:id, identityKeysPath:`orbitdb/ace-ac-test/identity-keys-path/${id}` })
        }

        // Create first entity connected to local database
        entityID1 = 'entityID1'
        entity1 = await DBInterfACE.createDBInterface(entityID1, ipfs1, localDatabase, (await idGen(entityID1)), {loki})

        // Create second entity connected to local database
        entityID2 = 'entityID2'
        entity2 = await DBInterfACE.createDBInterface(entityID2, ipfs1, localDatabase, (await idGen(entityID2)), {loki})

        // Create second entity connected to local database
        entityID3 = 'entityID3'
        entity3 = await DBInterfACE.createDBInterface(entityID3, ipfs1, localDatabase, (await idGen(entityID3)), {loki})
      })

      after(async () => {
        if(localDatabase) await localDatabase.drop()
      })

      it('e1 creates object - e2 and e3 can write, only e2 can delete', async function () {
        // The owner does not need to be included in the permission list
        // Has all permissions by default
        const writePermissions = [ entityID2, entityID3 ]
        const deletePermissions = [ entityID2 ]

        insertObj = {
          _id: 'obj1',
          data: 'e1 creates object', // actual object data
          permissions: { // list of permissions
            write: writePermissions, // list of users with write permission
            delete: deletePermissions // list of users with delete permission
          }
        }

        await entity1.create(insertObj)

        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 creates object')
      })

      it('e1 updates object', async () => {
        const newObj = {
          _id: 'obj1',
          data: 'e1 updates object',
          encrypted: false
        }

        await entity1.update(newObj)

        let retrievedObj = (await entity1.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 updates object')
      })

      it('e2 attempts to create object with same ID, but no permissions', async () => {
        // e2 gives no permissions to other entities
        const writePermissions = []
        const deletePermissions = []

        const newInsertObj = {
          _id: 'obj1',
          data: 'e2 creates object', // actual object data
          permissions: { // list of permissions
            write: writePermissions, // list of users with write permission
            delete: deletePermissions // list of users with delete permission
          }
        }

        await entity2.create(newInsertObj)

        // object state remains unchanged
        let retrievedObj = (await entity2.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e1 updates object')
      })

      it('e3 updates object', async () => {
        // e3 is able to update object because e2's `create` is ignored for now
        const newObj = {
          _id: 'obj1',
          data: 'e3 updates object',
          encrypted: false
        }

        await entity3.update(newObj)

        let retrievedObj = (await entity3.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data, 'e3 updates object')
      })

      it('e2 is declared the legitimate owner', async () => {
        // verify e2 as the legitimate owner of obj1
        await localDatabase.verifyObjectOwner('obj1', entityID2)

        let retrievedObj = (await localDatabase.get('obj1'))[0]
        assert.equal(retrievedObj._id, 'obj1')
        assert.equal(retrievedObj.data.data, 'e2 creates object')
      })
    })
  })
})
