/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const OrbitDB = require('orbit-db')

const ACENode = require('../src/ace-node')
const cryptoUtils = require('../src/crypto-utils')

const loki = { autosave: false }

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './ace-tests/sharekey/1'
const dbPath2 = './ace-tests/sharekey/2'
const dbPath3 = './ace-tests/sharekey/3'
const dbPath4 = './ace-tests/sharekey/4'
const ipfsPath1 = './ace-tests/sharekey/1/ipfs'
const ipfsPath2 = './ace-tests/sharekey/2/ipfs'
const ipfsPath3 = './ace-tests/sharekey/3/ipfs'
const ipfsPath4 = './ace-tests/sharekey/4/ipfs'

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const waitForReplication = async (log, stopSize, time = 50) => {
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      if (log.length >= stopSize) {
        clearInterval(interval)
        resolve()
      }
    }, time)
  })
}

const mockBlockchain = {
  isKeyInBlockchain: () => true
}

const KEYTIMEOUT = 1500

Object.keys(testAPIs).forEach(API => {
  describe(`eunomia-access-control - query methods (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfsd3, ipfsd4, ipfs1, ipfs2, ipfs3, ipfs4
    let orbitdb1, orbitdb2, orbitdb3, orbitdb4
    let db1, db2, db3, db4

    // Create two IPFS instances
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      config.daemon3 = Object.assign({}, config.daemon2, { repo: ipfsPath3 })
      config.daemon4 = Object.assign({}, config.daemon2, { repo: ipfsPath4 })
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(config.daemon3.repo)
      rmrf.sync(config.daemon4.repo)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfsd3 = await startIpfs(API, config.daemon3)
      ipfsd4 = await startIpfs(API, config.daemon4)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      ipfs3 = ipfsd3.api
      ipfs4 = ipfsd4.api

      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      await connectPeers(ipfs1, ipfs3)
      await connectPeers(ipfs2, ipfs3)

      // await connectPeers(ipfs1, ipfs4)
      // await connectPeers(ipfs2, ipfs4)
      await connectPeers(ipfs3, ipfs4)

      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })
      orbitdb3 = await OrbitDB.createInstance(ipfs3, { directory: dbPath3 })
      orbitdb4 = await OrbitDB.createInstance(ipfs4, { directory: dbPath4 })

      // This creates the local database
      const options = { create: true, accessController: { write: ['*'] } }
      db1 = await orbitdb1.docs('address', options)

      // Open the database on the other nodes
      const options2 = { sync: true }
      const address = db1.address.toString()
      db2 = await orbitdb2.docs(address, options2)
      db3 = await orbitdb3.docs(address, options2)
    })

    after(async () => {
      if (db1) await db1.drop()
      if (db2) await db2.drop()
      if (db3) await db3.drop()
      if (db4) await db4.drop()

      if(orbitdb1) await orbitdb1.stop()
      if(orbitdb2) await orbitdb2.stop()
      if(orbitdb3) await orbitdb3.stop()
      if(orbitdb4) await orbitdb4.stop()

      if (ipfsd1) await stopIpfs(ipfsd1)
      if (ipfsd2) await stopIpfs(ipfsd2)
      if (ipfsd3) await stopIpfs(ipfsd3)
      if (ipfsd4) await stopIpfs(ipfsd4)
    })

    describe('Query', function () {
      let node1, node2, node3, node4
      let dbI1, dbI2, dbI3, dbI4

      before(async function () {
        node1 = new ACENode(ipfs1, {blockchain: mockBlockchain, secretSharingThreshold: 2, loki})
        node2 = new ACENode(ipfs2, {blockchain: mockBlockchain, secretSharingThreshold: 2, loki})
        node3 = new ACENode(ipfs3, {blockchain: mockBlockchain, secretSharingThreshold: 2, loki})

        dbI1 = await node1.addDB(db1, {keyRetrievalTimeout: KEYTIMEOUT})
        dbI2 = await node2.addDB(db2, {keyRetrievalTimeout: KEYTIMEOUT})
        dbI3 = await node3.addDB(db3, {keyRetrievalTimeout: KEYTIMEOUT})

        dbI2.addPublicKey(node1.id, node1.publicKey)
        dbI3.addPublicKey(node1.id, node1.publicKey)
        dbI1.addPublicKey(node2.id, node2.publicKey)
        dbI3.addPublicKey(node2.id, node2.publicKey)
        dbI1.addPublicKey(node3.id, node3.publicKey)
        dbI2.addPublicKey(node3.id, node3.publicKey)

        // Give time for pubsub to initialize
        await sleep(250)
      })

      after(async function () {
        await sleep(1000)
      })

      describe('3 ACE Nodes (then a 4th joins)', function () {
        let obj

        it('should create and send key shares to ACE nodes', async function () {
          obj = {
            _id: 'this-is-a-unique-ID',
            text: 'very important data',
            permissions: { write: [], delete: [], read: ['*'] }
          }
          const totalShares = 3
          const threshold = 2

          await dbI1.create(obj, totalShares, threshold)

          // Give enough time for shares to reach ACE Nodes
          await waitForReplication(db1._oplog, 1)
        })

        it('should be able to request, receive and reassemble key', async function () {
          // Open the database on the 4th node
          const opts = { sync: true }
          const address = db1.address.toString()
          db4 = await orbitdb4.docs(address, opts)

          node4 = new ACENode(ipfs4, {blockchain: mockBlockchain, secretSharingThreshold: 2, loki})
          dbI4 = await node4.addDB(db4, {keyRetrievalTimeout: KEYTIMEOUT})
          dbI4.addPublicKey(node1.id, node1.publicKey)
          dbI4.addPublicKey(node2.id, node2.publicKey)
          dbI4.addPublicKey(node3.id, node3.publicKey)

          // Add node4's key to other nodes so they can verify its requests
          dbI1.addPublicKey(node4.id, node4.publicKey)
          dbI2.addPublicKey(node4.id, node4.publicKey)
          dbI3.addPublicKey(node4.id, node4.publicKey)

          // ATTENTION!! Nodes in the path only relay messages if they are subscribed to the message's topic

          await waitForReplication(db2._oplog, 1)
          await waitForReplication(db3._oplog, 1)
          await waitForReplication(db4._oplog, 1)
        })

        it('should be able to update object', async function () {
          const updateObj = {
            _id: 'this-is-a-unique-ID',
            text: 'not important data'
          }
          await dbI1.update(updateObj)

          await waitForReplication(db2._oplog, 2)
          await waitForReplication(db3._oplog, 2)
          await waitForReplication(db4._oplog, 2)
        })

        it('should be able to delete object', async function () {
          await dbI1.del(obj._id)

          await waitForReplication(db2._oplog, 3)
          await waitForReplication(db3._oplog, 3)
          await waitForReplication(db4._oplog, 3)
        })

        it('should get non-encrypted object', async function () {
          const nonEncryptedObj = {
            _id: 'asdwqed',
            text: 'lotsofdata',
            permissions: { write: [], delete: [] }
          }

          const logSize = db1._oplog.length
          await dbI1.create(nonEncryptedObj)

          await waitForReplication(db1._oplog, logSize+1)
          await waitForReplication(db2._oplog, logSize+1)
          await waitForReplication(db3._oplog, logSize+1)
          await waitForReplication(db4._oplog, logSize+1)
        })

        it('should repeat test for a new object', async function () {
          obj = {
            _id: 'this-is-another-unique-ID',
            text: 'much more important data',
            permissions: { write: [], delete: [], read: ['*'] }
          }
          const totalShares = 3
          const threshold = 2

          const logSize = db1._oplog.length
          await dbI1.create(obj, totalShares, threshold)

          // Give enough time for shares to reach ACE Nodes
          await waitForReplication(db1._oplog, logSize+1)
          await waitForReplication(db2._oplog, logSize+1)
          await waitForReplication(db3._oplog, logSize+1)
          await waitForReplication(db4._oplog, logSize+1)
        })

        it('unauthorized node (3) should not be able to read object', async function () {
          const obj = {
            _id: 'q12k34idns',
            text: 'lotsofimpressssivedata',
            cleartextProperties: { blabla: 'blaeh' },
            permissions: { write: [], delete: [], read: [node2.id, node4.id] }
          }

          const logSize = db1._oplog.length
          await dbI1.create(obj)

          await waitForReplication(db1._oplog, logSize+1)
          await waitForReplication(db2._oplog, logSize+1)
          await waitForReplication(db3._oplog, logSize+1)
          await waitForReplication(db4._oplog, logSize+1)
        })
      })

      describe('keeping key local', () => {
        let obj
        it('creates object with totalShares and threshold below 2 - prints error', async () => {
          obj = {
            _id: 'this-is-not-a-unique-ID',
            text: 'not at all important data',
            permissions: { write: [], delete: [], read: ['*'] }
          }
          const totalShares = 1
          const threshold = 1

          const prevLogLen = db1._oplog.length
          await dbI1.create(obj, totalShares, threshold)
          await waitForReplication(db1._oplog, prevLogLen+1)
        })
      })

      describe('querying methods', () => {
        describe('`query` method', () => {
          it('get everything', async () => {
            const results1 = await dbI1.query(() => true)
            assert.deepStrictEqual(results1.length, 4)
            assert(results1.map(e => e._id).includes('asdwqed'))
            assert(results1.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results1.map(e => e._id).includes('q12k34idns'))
            assert(results1.map(e => e._id).includes('this-is-not-a-unique-ID'))

            const results2 = await dbI2.query(() => true)
            assert.deepStrictEqual(results2.length, 3)
            assert(results2.map(e => e._id).includes('asdwqed'))
            assert(results2.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results2.map(e => e._id).includes('q12k34idns'))

            const results3 = await dbI3.query(() => true)
            assert.deepStrictEqual(results3.length, 2)
            assert(results3.map(e => e._id).includes('asdwqed'))
            assert(results3.map(e => e._id).includes('this-is-another-unique-ID'))

            const results4 = await dbI4.query(() => true)
            assert.deepStrictEqual(results4.length, 3)
            assert(results4.map(e => e._id).includes('asdwqed'))
            assert(results4.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results4.map(e => e._id).includes('q12k34idns'))
          })

          it('id includes "unique"', async () => {
            const results1 = await dbI1.query((e) => e._id.includes('unique'))
            assert.deepStrictEqual(results1.length, 2)
            assert(results1.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results1.map(e => e._id).includes('this-is-not-a-unique-ID'))

            const results2 = await dbI2.query((e) => e._id.includes('unique'))
            assert.deepStrictEqual(results2.length, 1)
            assert(results2.map(e => e._id).includes('this-is-another-unique-ID'))

            const results3 = await dbI3.query((e) => e._id.includes('unique'))
            assert.deepStrictEqual(results3.length, 1)
            assert(results3.map(e => e._id).includes('this-is-another-unique-ID'))

            const results4 = await dbI4.query((e) => e._id.includes('unique'))
            assert.deepStrictEqual(results4.length, 1)
            assert(results4.map(e => e._id).includes('this-is-another-unique-ID'))
          })

          it('id includes "dns"', async () => {
            const results1 = await dbI1.query((e) => e._id.includes('dns'))
            assert.deepStrictEqual(results1.length, 1)
            assert(results1.map(e => e._id).includes('q12k34idns'))

            const results2 = await dbI2.query((e) => e._id.includes('dns'))
            assert.deepStrictEqual(results2.length, 1)
            assert(results2.map(e => e._id).includes('q12k34idns'))

            const results3 = await dbI3.query((e) => e._id.includes('dns'))
            assert.deepStrictEqual(results3.length, 0)

            const results4 = await dbI4.query((e) => e._id.includes('dns'))
            assert.deepStrictEqual(results4.length, 1)
            assert(results4.map(e => e._id).includes('q12k34idns'))
          })

          it('text === "lotsofimpressssivedata"', async () => {
            const results1 = await dbI1.query((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results1.length, 0)

            const results2 = await dbI2.query((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results2.length, 0)

            const results3 = await dbI3.query((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results3.length, 0)

            const results4 = await dbI4.query((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results4.length, 0)
          })

          it('cleartextProperties.blabla === "blaeh"', async () => {
            const queryExp = (e) => e.cleartextProperties && e.cleartextProperties.blabla === 'blaeh'

            const results1 = await dbI1.query(queryExp)
            assert.deepStrictEqual(results1.length, 1)
            assert.deepStrictEqual(results1[0]._id, 'q12k34idns')
            assert.deepStrictEqual(results1[0].text, 'lotsofimpressssivedata')

            const results2 = await dbI2.query(queryExp)
            assert.deepStrictEqual(results2.length, 1)
            assert.deepStrictEqual(results2[0]._id, 'q12k34idns')
            assert.deepStrictEqual(results2[0].text, 'lotsofimpressssivedata')

            const results3 = await dbI3.query(queryExp)
            assert.deepStrictEqual(results3.length, 0)

            const results4 = await dbI4.query(queryExp)
            assert.deepStrictEqual(results4.length, 1)
            assert.deepStrictEqual(results4[0]._id, 'q12k34idns')
            assert.deepStrictEqual(results4[0].text, 'lotsofimpressssivedata')
          })
        })

        describe('`queryAll` method', () => {
          it('get everything', async () => {
            const results1 = await dbI1.queryAll(() => true)
            assert.deepStrictEqual(results1.length, 4)
            assert(results1.map(e => e._id).includes('asdwqed'))
            assert(results1.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results1.map(e => e._id).includes('q12k34idns'))
            assert(results1.map(e => e._id).includes('this-is-not-a-unique-ID'))

            const results2 = await dbI2.queryAll(() => true)
            assert.deepStrictEqual(results2.length, 3)
            assert(results2.map(e => e._id).includes('asdwqed'))
            assert(results2.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results2.map(e => e._id).includes('q12k34idns'))

            const results3 = await dbI3.queryAll(() => true)
            assert.deepStrictEqual(results3.length, 2)
            assert(results3.map(e => e._id).includes('asdwqed'))
            assert(results3.map(e => e._id).includes('this-is-another-unique-ID'))

            const results4 = await dbI4.queryAll(() => true)
            assert.deepStrictEqual(results4.length, 3)
            assert(results4.map(e => e._id).includes('asdwqed'))
            assert(results4.map(e => e._id).includes('this-is-another-unique-ID'))
            assert(results4.map(e => e._id).includes('q12k34idns'))
          })

          it('text === "lotsofimpressssivedata"', async () => {
            const results1 = await dbI1.queryAll((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results1.length, 1)

            const results2 = await dbI2.queryAll((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results2.length, 1)

            const results3 = await dbI3.queryAll((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results3.length, 0)

            const results4 = await dbI4.queryAll((e) => e.text === 'lotsofimpressssivedata')
            assert.deepStrictEqual(results4.length, 1)
          })
        })
      })
    })
  })
})
