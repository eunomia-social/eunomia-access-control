/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const crypto = require('../src/crypto-utils')
const assert = require('assert')

describe('eunomia-access-control - crypto-utils - keygen, encrypt, and decrypt', () => {
  let password, key, plaintext, parsedKey

  before(() => {
    password = 'this is a very easy password'

    const objToEncrypt = {
      ab: 'tesdcs',
      asd: '123321ol'
    }
    plaintext = JSON.stringify(objToEncrypt)
  })

  it('should generate a valid key from password', () => {
    key = crypto.keygen(password)
    assert(key instanceof Buffer)
  })

  it('should encrypt and decrypt correctly using key', () => {
    assert.deepStrictEqual(crypto.decrypt(key, crypto.encrypt(key, plaintext)), plaintext)
  })

  it('should encrypt and decrypt correctly using password', () => {
    assert.deepStrictEqual(crypto.pwDecrypt(password, crypto.pwEncrypt(password, plaintext)), plaintext)
  })

  it('should correctly serialize and deserialize key', () => {
    const keyString = crypto.serializeKey(key)
    assert.deepStrictEqual(typeof(keyString), 'string')

    parsedKey = crypto.deserializeKey(keyString)
    assert.deepStrictEqual(key, parsedKey)
  })

  it('should encrypt and decrypt correctly using deserialized key', () => {
    assert.deepStrictEqual(crypto.decrypt(parsedKey, crypto.encrypt(parsedKey, plaintext)), plaintext)
  })
})
