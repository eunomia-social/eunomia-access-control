/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const crypto = require('../src/crypto-utils')
const assert = require('assert')

describe('eunomia-access-control - crypto-utils - secret sharing', () => {
  let secret, shares

  before(() => {
    secret = 'this is a very secret secret'
  })

  it('should split secret into 10 shares (threshold of 4)', () => {
    // crypto.split(secret, totalShares, threshold)
    shares = crypto.split(secret, 10, 4)
    assert.equal(shares.length, 10)
  })

  it('should not reassemble secret from 3 shares', () => {
    assert.notDeepStrictEqual(crypto.combine(shares.slice(0,3)), secret)
    assert.notDeepStrictEqual(crypto.combine(shares.slice(1,4)), secret)
    assert.notDeepStrictEqual(crypto.combine(shares.slice(2,5)), secret)
    assert.notDeepStrictEqual(crypto.combine(shares.slice(3,6)), secret)
    assert.notDeepStrictEqual(crypto.combine(shares.slice(4,7)), secret)
    assert.notDeepStrictEqual(crypto.combine(shares.slice(7,10)), secret)
  })

  it('should reassemble secret from 4 different shares', () => {
    assert.deepStrictEqual(crypto.combine(shares.slice(0,4)), secret)
    assert.deepStrictEqual(crypto.combine(shares.slice(1,5)), secret)
    assert.deepStrictEqual(crypto.combine(shares.slice(2,6)), secret)
    assert.deepStrictEqual(crypto.combine(shares.slice(3,7)), secret)
    assert.deepStrictEqual(crypto.combine(shares.slice(6,10)), secret)
  })

  it('should reassemble secret from all shares', () => {
    assert.deepStrictEqual(crypto.combine(shares), secret)
  })

  it('should not reassemble secret from 4 equal shares', () => {
    assert.notDeepStrictEqual(crypto.combine([shares[0], shares[0], shares[0], shares[0]]), secret)
  })

  it('should generate new valid shares from 4 different shares', () => {
    let newShare11 = crypto.newShare(11, shares.slice(0,4))
    assert.deepStrictEqual(crypto.combine([shares[0], shares[1], shares[2], newShare11]), secret)

    let newShare2 = crypto.newShare(2, shares.slice(0,4))
    assert.deepStrictEqual(crypto.combine([shares[0], shares[2], shares[3], newShare2]), secret)
  })

  it('should fail when splitting secret into a single share', () => {
    assert.throws(
      () => crypto.split(secret, 1, 1),
      {
        name: 'Error',
        message: 'Number of shares must be an integer between 2 and 2^bits-1 (255), inclusive.'
      }
    )
  })

  it('should fail when giving threshold of 1', () => {
    assert.throws(
      () => crypto.split(secret, 4, 1),
      {
        name: 'Error',
        message: 'Threshold number of shares must be an integer between 2 and 2^bits-1 (255), inclusive.'
      }
    )
  })

  it('should successfully split secret into 2 shares', () => {
    shares = crypto.split(secret, 2, 2)
    assert.equal(shares.length, 2)
    assert.deepStrictEqual(crypto.combine(shares), secret)
  })
})
