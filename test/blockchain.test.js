/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const OrbitDB = require('orbit-db')

const Blockchain = require('../src/blockchain')
const ACENode = require('../src/ace-node')

const BLOCKCHAIN_URL = 'http://localhost:8973'
const loki = { autosave: false }

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './ace-tests/blockchain/1'
const dbPath2 = './ace-tests/blockchain/2'
const dbPath3 = './ace-tests/blockchain/3'
const dbPath4 = './ace-tests/blockchain/4'
const ipfsPath1 = './ace-tests/blockchain/1/ipfs'
const ipfsPath2 = './ace-tests/blockchain/2/ipfs'
const ipfsPath3 = './ace-tests/blockchain/3/ipfs'
const ipfsPath4 = './ace-tests/blockchain/4/ipfs'

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const waitForReplication = async (log, stopSize, time = 50) => {
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      if (log.length >= stopSize) {
        clearInterval(interval)
        resolve()
      }
    }, time)
  })
}

Object.keys(testAPIs).forEach(API => {
  describe(`eunomia-access-control - blockchain (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfsd3, ipfsd4, ipfs1, ipfs2, ipfs3, ipfs4
    let orbitdb1, orbitdb2, orbitdb3, orbitdb4
    let db1, db2, db3, db4

    // Create two IPFS instances
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      config.daemon3 = Object.assign({}, config.daemon2, { repo: ipfsPath3 })
      config.daemon4 = Object.assign({}, config.daemon2, { repo: ipfsPath4 })
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(config.daemon3.repo)
      rmrf.sync(config.daemon4.repo)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfsd3 = await startIpfs(API, config.daemon3)
      ipfsd4 = await startIpfs(API, config.daemon4)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      ipfs3 = ipfsd3.api
      ipfs4 = ipfsd4.api

      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      await connectPeers(ipfs1, ipfs3)
      await connectPeers(ipfs2, ipfs3)

      await connectPeers(ipfs1, ipfs4)
      await connectPeers(ipfs2, ipfs4)
      await connectPeers(ipfs3, ipfs4)

      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })
      orbitdb3 = await OrbitDB.createInstance(ipfs3, { directory: dbPath3 })
      orbitdb4 = await OrbitDB.createInstance(ipfs4, { directory: dbPath4 })

      // This creates the local database
      const options = { create: true, accessController: { write: ['*'] } }
      db1 = await orbitdb1.docs('address', options)

      // Open the database on the other nodes
      const options2 = { sync: true }
      const address = db1.address.toString()
      db2 = await orbitdb2.docs(address, options2)
      db3 = await orbitdb3.docs(address, options2)
      db4 = await orbitdb4.docs(address, options2)
    })

    after(async () => {
      if (db1) await db1.drop()
      if (db2) await db2.drop()
      if (db3) await db3.drop()
      if (db4) await db4.drop()

      if(orbitdb1) await orbitdb1.stop()
      if(orbitdb2) await orbitdb2.stop()
      if(orbitdb3) await orbitdb3.stop()
      if(orbitdb4) await orbitdb4.stop()

      if (ipfsd1) await stopIpfs(ipfsd1)
      if (ipfsd2) await stopIpfs(ipfsd2)
      if (ipfsd3) await stopIpfs(ipfsd3)
      if (ipfsd4) await stopIpfs(ipfsd4)
    })

    describe('blockchain only', async function () {
      let publicKeyType, bc
      let key1, key2, key3

      before(async function () {
        publicKeyType = 'BC-ACE-TEST-' + Math.random().toString(16).substring(7)
        console.log(`publicKeyType: ${publicKeyType}`)

        bc = new Blockchain(BLOCKCHAIN_URL, publicKeyType)

        const keys = await bc.getPublicKeys()
        assert.equal(keys.length, 0)

        const uniqueKeys = await bc.getUniquePublicKeys()
        assert.equal(uniqueKeys.length, 0)
      })

      it('inserts 3 keys in the blockchain', async function () {
        key1 = { id: 'id1', publicKey: 'publicKey1' }
        assert(!await bc.isKeyInBlockchain(key1.id, key1.publicKey))
        await bc.postPublicKey(key1.id, key1.publicKey)
        assert(await bc.isKeyInBlockchain(key1.id, key1.publicKey))

        key2 = { id: 'id2', publicKey: 'publicKey2' }
        assert(!await bc.isKeyInBlockchain(key2.id, key2.publicKey))
        await bc.postPublicKey(key2.id, key2.publicKey)
        assert(await bc.isKeyInBlockchain(key2.id, key2.publicKey))

        key3 = { id: 'id3', publicKey: 'publicKey3' }
        assert(!await bc.isKeyInBlockchain(key3.id, key3.publicKey))
        await bc.postPublicKey(key3.id, key3.publicKey)
        assert(await bc.isKeyInBlockchain(key3.id, key3.publicKey))

        assert(await bc.isKeyInBlockchain(key1.id, key1.publicKey))
        assert(await bc.isKeyInBlockchain(key2.id, key2.publicKey))
        assert(await bc.isKeyInBlockchain(key3.id, key3.publicKey))
      })

      it('correctly retrieves all the keys from the blockchain', async function () {
        const keys = await bc.getUniquePublicKeys()

        assert.equal(keys.length, 3)
        assert.deepStrictEqual(keys[0], key1)
        assert.deepStrictEqual(keys[1], key2)
        assert.deepStrictEqual(keys[2], key3)
      })
    })

    describe('3 ACE Nodes', function () {
      let node1, node2, node3, node4, blockchainPublicKeyType
      let dbI1, dbI2, dbI3

      before(async function () {
        blockchainPublicKeyType = 'BC-ACE-TEST-' + Math.random().toString(16).substring(7)

        node1 = new ACENode(ipfs1, {blockchainPublicKeyType, blockchainUrl: BLOCKCHAIN_URL, loki})
        node2 = new ACENode(ipfs2, {blockchainPublicKeyType, blockchainUrl: BLOCKCHAIN_URL, loki})
        node3 = new ACENode(ipfs3, {blockchainPublicKeyType, blockchainUrl: BLOCKCHAIN_URL, loki})

        dbI1 = await node1.addDB(db1)
        dbI2 = await node2.addDB(db2)
        dbI3 = await node3.addDB(db3)

        // Give time for pubsub to initialize
        await sleep(250)
      })

      after(async function () {
        await sleep(1000)
      })

      it('all nodes should be registered in the blockchain', async function () {
        assert(await node1.blockchain.isKeyInBlockchain(node1.id, node1.publicKey))
        assert(await node1.blockchain.isKeyInBlockchain(node2.id, node2.publicKey))
        assert(await node1.blockchain.isKeyInBlockchain(node3.id, node3.publicKey))

        assert(await node2.blockchain.isKeyInBlockchain(node1.id, node1.publicKey))
        assert(await node2.blockchain.isKeyInBlockchain(node2.id, node2.publicKey))
        assert(await node2.blockchain.isKeyInBlockchain(node3.id, node3.publicKey))

        assert(await node3.blockchain.isKeyInBlockchain(node1.id, node1.publicKey))
        assert(await node3.blockchain.isKeyInBlockchain(node2.id, node2.publicKey))
        assert(await node3.blockchain.isKeyInBlockchain(node3.id, node3.publicKey))
      })

      it('all nodes update locally stored public key', async function () {
        await node1.updatePublicKeys()
        await node2.updatePublicKeys()
        await node3.updatePublicKeys()

        let key1 = dbI1.db._index.publicKeys.get(node1.id)
        let key2 = dbI1.db._index.publicKeys.get(node2.id)
        let key3 = dbI1.db._index.publicKeys.get(node3.id)
        assert.deepStrictEqual(key1, node1.publicKey)
        assert.deepStrictEqual(key2, node2.publicKey)
        assert.deepStrictEqual(key3, node3.publicKey)

        key1 = dbI2.db._index.publicKeys.get(node1.id)
        key2 = dbI2.db._index.publicKeys.get(node2.id)
        key3 = dbI2.db._index.publicKeys.get(node3.id)
        assert.deepStrictEqual(key1, node1.publicKey)
        assert.deepStrictEqual(key2, node2.publicKey)
        assert.deepStrictEqual(key3, node3.publicKey)

        key1 = dbI3.db._index.publicKeys.get(node1.id)
        key2 = dbI3.db._index.publicKeys.get(node2.id)
        key3 = dbI3.db._index.publicKeys.get(node3.id)
        assert.deepStrictEqual(key1, node1.publicKey)
        assert.deepStrictEqual(key2, node2.publicKey)
        assert.deepStrictEqual(key3, node3.publicKey)
      })

      it('should update public keys again without throwing exceptions', async function () {
        await node1.updatePublicKeys()
        await node2.updatePublicKeys()
        await node3.updatePublicKeys()
      })

      let obj

      it('should create and send key shares to ACE nodes', async function () {
        obj = {
          _id: 'this-is-a-unique-ID',
          data: 'very important data',
          permissions: { write: [], delete: [] }
        }
        const totalShares = 3
        const threshold = 2

        await dbI1.create(obj, totalShares, threshold)

        await waitForReplication(db1._oplog, 1)
        assert.equal(db1._oplog.length, 1)

        await waitForReplication(db2._oplog, 1)
        let retrievedObj = (await dbI2.get(obj._id))[0]
        assert.deepStrictEqual(obj, retrievedObj)

        await waitForReplication(db3._oplog, 1)
        retrievedObj = (await dbI3.get(obj._id))[0]
        assert.deepStrictEqual(obj, retrievedObj)
      })

      it('node4 joins and updates keys automatically - using update period', async () => {
        node4 = new ACENode(ipfs4, {blockchainPublicKeyType, blockchainUrl: BLOCKCHAIN_URL, keyUpdatePeriod: 250, loki})
        const dbI4 = await node4.addDB(db4)

        return new Promise((resolve)=>{
          setTimeout(() => {
            clearInterval(node4.keyUpdateIntervalObj)
            const key1 = dbI4.db._index.publicKeys.get(node1.id)
            const key2 = dbI4.db._index.publicKeys.get(node2.id)
            const key3 = dbI4.db._index.publicKeys.get(node3.id)
            const key4 = dbI4.db._index.publicKeys.get(node4.id)
            assert.deepStrictEqual(key1, node1.publicKey)
            assert.deepStrictEqual(key2, node2.publicKey)
            assert.deepStrictEqual(key3, node3.publicKey)
            assert.deepStrictEqual(key4, node4.publicKey)
            resolve()
          }, 750)
        })
      })
    })
  })
})
