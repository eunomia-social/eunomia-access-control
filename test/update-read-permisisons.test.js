/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const OrbitDB = require('orbit-db')

const ACENode = require('../src/ace-node')
const cryptoUtils = require('../src/crypto-utils')

const loki = { autosave: false }

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './ace-tests/update-read-permissions/1'
const dbPath2 = './ace-tests/update-read-permissions/2'
const dbPath3 = './ace-tests/update-read-permissions/3'
const dbPath4 = './ace-tests/update-read-permissions/4'
const ipfsPath1 = './ace-tests/update-read-permissions/1/ipfs'
const ipfsPath2 = './ace-tests/update-read-permissions/2/ipfs'
const ipfsPath3 = './ace-tests/update-read-permissions/3/ipfs'
const ipfsPath4 = './ace-tests/update-read-permissions/4/ipfs'

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const waitForReplication = async (log, stopSize, time = 50) => {
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      if (log.length >= stopSize) {
        clearInterval(interval)
        resolve()
      }
    }, time)
  })
}

const mockBlockchain = {
  isKeyInBlockchain: () => true
}

const KEYTIMEOUT = 1500

Object.keys(testAPIs).forEach(API => {
  describe(`eunomia-access-control - updating read permissions (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfsd3, ipfsd4, ipfs1, ipfs2, ipfs3, ipfs4
    let orbitdb1, orbitdb2, orbitdb3, orbitdb4
    let db1, db2, db3, db4

    // Create two IPFS instances
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      config.daemon3 = Object.assign({}, config.daemon2, { repo: ipfsPath3 })
      config.daemon4 = Object.assign({}, config.daemon2, { repo: ipfsPath4 })
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(config.daemon3.repo)
      rmrf.sync(config.daemon4.repo)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfsd3 = await startIpfs(API, config.daemon3)
      ipfsd4 = await startIpfs(API, config.daemon4)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      ipfs3 = ipfsd3.api
      ipfs4 = ipfsd4.api

      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      await connectPeers(ipfs1, ipfs3)
      await connectPeers(ipfs2, ipfs3)

      // await connectPeers(ipfs1, ipfs4)
      // await connectPeers(ipfs2, ipfs4)
      await connectPeers(ipfs3, ipfs4)

      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })
      orbitdb3 = await OrbitDB.createInstance(ipfs3, { directory: dbPath3 })
      orbitdb4 = await OrbitDB.createInstance(ipfs4, { directory: dbPath4 })

      // This creates the local database
      const options = { create: true, accessController: { write: ['*'] } }
      db1 = await orbitdb1.docs('address', options)

      // Open the database on the other nodes
      const options2 = { sync: true }
      const address = db1.address.toString()
      db2 = await orbitdb2.docs(address, options2)
      db3 = await orbitdb3.docs(address, options2)
    })

    after(async () => {
      if (db1) await db1.drop()
      if (db2) await db2.drop()
      if (db3) await db3.drop()
      if (db4) await db4.drop()

      if(orbitdb1) await orbitdb1.stop()
      if(orbitdb2) await orbitdb2.stop()
      if(orbitdb3) await orbitdb3.stop()
      if(orbitdb4) await orbitdb4.stop()

      if (ipfsd1) await stopIpfs(ipfsd1)
      if (ipfsd2) await stopIpfs(ipfsd2)
      if (ipfsd3) await stopIpfs(ipfsd3)
      if (ipfsd4) await stopIpfs(ipfsd4)
    })

    describe('Sharing keys', function () {
      let node1, node2, node3, node4
      let dbI1, dbI2, dbI3, dbI4
      let shareNode1, shareNode2, shareNode3

      before(async function () {
        node1 = new ACENode(ipfs1, {blockchain: mockBlockchain, secretSharingThreshold: 2, id: 'updatereadperm1', loki})
        node2 = new ACENode(ipfs2, {blockchain: mockBlockchain, secretSharingThreshold: 2, id: 'updatereadperm2', loki})
        node3 = new ACENode(ipfs3, {blockchain: mockBlockchain, secretSharingThreshold: 2, id: 'updatereadperm3', loki})

        dbI1 = await node1.addDB(db1, {keyRetrievalTimeout: KEYTIMEOUT})
        dbI2 = await node2.addDB(db2, {keyRetrievalTimeout: KEYTIMEOUT})
        dbI3 = await node3.addDB(db3, {keyRetrievalTimeout: KEYTIMEOUT})

        dbI2.addPublicKey(node1.id, node1.publicKey)
        dbI3.addPublicKey(node1.id, node1.publicKey)
        dbI1.addPublicKey(node2.id, node2.publicKey)
        dbI3.addPublicKey(node2.id, node2.publicKey)
        dbI1.addPublicKey(node3.id, node3.publicKey)
        dbI2.addPublicKey(node3.id, node3.publicKey)

        // Give time for pubsub to initialize
        await sleep(250)
      })

      after(async function () {
        await sleep(1000)
        if (node1) node1.close()
        if (node2) node2.close()
        if (node3) node3.close()
        if (node4) node4.close()
      })

      describe('3 ACE Nodes (then a 4th joins)', function () {
        let obj

        it('should create object (only node2 can read)', async function () {
          obj = {
            _id: 'this-is-a-unique-ID',
            text: 'very important data',
            permissions: { write: [], delete: [], read: [node2.id] }
          }
          await dbI1.create(obj)

          // Give enough time for shares to reach ACE Nodes
          await waitForReplication(db1._oplog, 1)
          assert.equal(db1._oplog.length, 1)
          await waitForReplication(db2._oplog, 1)
          assert.equal(db2._oplog.length, 1)
          await waitForReplication(db3._oplog, 1)
          assert.equal(db3._oplog.length, 1)

          shareNode1 = dbI1.shares.get(obj._id).share
          shareNode2 = dbI2.shares.get(obj._id).share
          shareNode3 = dbI2.shares.get(obj._id).share
          assert(shareNode1)
          assert(shareNode2)
          assert(shareNode3)
        })

        it('should allow node2 to read, and forbid node3', async function () {
          let retrievedObjs = await dbI1.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])

          retrievedObjs = await dbI2.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])

          // Node 3 fails to read object
          retrievedObjs = await dbI3.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 0)
        })

        it('should change read permissions (only node3 can read)', async function () {
          obj = {
            _id: 'this-is-a-unique-ID',
            text: 'very important data',
            permissions: { write: [], delete: [], read: [node3.id] }
          }
          await dbI1.update(obj)

          // Give enough time for shares to reach ACE Nodes
          await waitForReplication(db1._oplog, 2)
          assert.equal(db1._oplog.length, 2)
          await waitForReplication(db2._oplog, 2)
          assert.equal(db2._oplog.length, 2)
          await waitForReplication(db3._oplog, 2)
          assert.equal(db3._oplog.length, 2)

          assert.notDeepStrictEqual(shareNode1, dbI1.shares.get(obj._id).share)
          assert.notDeepStrictEqual(shareNode2, dbI2.shares.get(obj._id).share)
          assert.notDeepStrictEqual(shareNode3, dbI3.shares.get(obj._id).share)
          shareNode1 = dbI1.shares.get(obj._id).share
          shareNode2 = dbI2.shares.get(obj._id).share
          shareNode3 = dbI2.shares.get(obj._id).share
        })

        it('should forbid node2 to read, and allow node3', async function () {
          let retrievedObjs = await dbI1.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])

          retrievedObjs = await dbI2.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 0)

          // Node 3 fails to read object
          retrievedObjs = await dbI3.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])
        })

        it('should change read permissions (all can read)', async function () {
          obj = {
            _id: 'this-is-a-unique-ID',
            text: 'very important data',
            permissions: { write: [], delete: [], read: ['*'] }
          }
          await dbI1.update(obj)

          // Give enough time for shares to reach ACE Nodes
          await waitForReplication(db1._oplog, 3)
          assert.equal(db1._oplog.length, 3)
          await waitForReplication(db2._oplog, 3)
          assert.equal(db2._oplog.length, 3)
          await waitForReplication(db3._oplog, 3)
          assert.equal(db3._oplog.length, 3)

          assert.notDeepStrictEqual(shareNode1, dbI1.shares.get(obj._id).share)
          assert.notDeepStrictEqual(shareNode2, dbI2.shares.get(obj._id).share)
          assert.notDeepStrictEqual(shareNode3, dbI3.shares.get(obj._id).share)
        })

        it('all nodes should be able to read', async function () {
          let retrievedObjs = await dbI1.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])

          retrievedObjs = await dbI2.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])

          // Node 3 fails to read object
          retrievedObjs = await dbI3.get(obj._id)
          assert.deepStrictEqual(retrievedObjs.length, 1)
          assert.deepStrictEqual(obj, retrievedObjs[0])
        })
      })
    })
  })
})
