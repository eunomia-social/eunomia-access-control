/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const PubSub = require('../src/pubsub')
const EventEmitter = require('events')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const ipfsPath1 = './ipfs-tests/pubsub/1/ipfs'
const ipfsPath2 = './ipfs-tests/pubsub/2/ipfs'
const ipfsPath3 = './ipfs-tests/pubsub/3/ipfs'

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const genStringByLength = (sizeBytes, char='a') => new Array(sizeBytes + 1).join(char)

Object.keys(testAPIs).forEach(API => {
  describe(`eunomia-access-control - ace-node pubsub (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfsd3, ipfs1, ipfs2, ipfs3

    // Create two IPFS instances
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      config.daemon3 = Object.assign({}, config.daemon2, { repo: ipfsPath3 })
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(config.daemon3.repo)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfsd3 = await startIpfs(API, config.daemon3)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      ipfs3 = ipfsd3.api

      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      await connectPeers(ipfs1, ipfs3)
      await connectPeers(ipfs2, ipfs3)
    })

    after(async () => {
      if (ipfsd1)
        await stopIpfs(ipfsd1)

      if (ipfsd2)
        await stopIpfs(ipfsd2)

      if (ipfsd3)
        await stopIpfs(ipfsd3)
    })

    describe('basic pubsub test', function () {
      let node1, node2, node3, events
      let numberOfMessages, expectedMessages

      const handleMessageEvent = (message) => function (topic, content, from) {
        numberOfMessages++
        assert.deepStrictEqual(JSON.stringify(content), JSON.stringify(message))
        assert.deepStrictEqual(from, node3.id)
        if (numberOfMessages >= expectedMessages) {
          events.emit('done')
        }
      }

      before(async function () {
        node1 = new PubSub(ipfs1)
        node2 = new PubSub(ipfs2)
        node3 = new PubSub(ipfs3)

        events = new EventEmitter()
      })

      it('node1 and node2 subscribe topic, node3 publishes message', async function () {
        const topic = 'blayeahwhatsure'
        const message = 'tell me something'

        numberOfMessages = 0
        expectedMessages = 2

        await node1.subscribe(topic, handleMessageEvent(message).bind(node1))
        let topics = node1._pubsub.ls()
        assert(topics.includes(topic))

        await node2.subscribe(topic, handleMessageEvent(message).bind(node2))
        topics = node2._pubsub.ls()
        assert(topics.includes(topic))

        return new Promise(async resolve => {
          events.on('done', resolve)
          await sleep(250)
          await node3.publish(topic, message)
        })
      })

      it('all nodes subscribe topic, node3 publishes message', async function () {
        const topic = 'thisoneisadifferentone'
        const message = 'tell me something'

        numberOfMessages = 0
        expectedMessages = 2

        await node1.subscribe(topic, handleMessageEvent(message).bind(node1))
        let topics = node1._pubsub.ls()
        assert(topics.includes(topic))

        await node2.subscribe(topic, handleMessageEvent(message).bind(node2))
        topics = node2._pubsub.ls()
        assert(topics.includes(topic))

        await node3.subscribe(topic, handleMessageEvent(message).bind(node3))
        topics = node3._pubsub.ls()
        assert(topics.includes(topic))

        return new Promise(async resolve => {
          events.on('done', resolve)
          await sleep(250)
          await node3.publish(topic, message)
        })
      })

      it('testing message size limit - very long string', async function () {
        const message = genStringByLength(15*1000*1000)
        const topic = 'veryLooongString'

        numberOfMessages = 0
        expectedMessages = 2

        await node1.subscribe(topic, handleMessageEvent(message).bind(node1))
        let topics = node1._pubsub.ls()
        assert(topics.includes(topic))

        await node2.subscribe(topic, handleMessageEvent(message).bind(node2))
        topics = node2._pubsub.ls()
        assert(topics.includes(topic))

        await node3.subscribe(topic, handleMessageEvent(message).bind(node3))
        topics = node3._pubsub.ls()
        assert(topics.includes(topic))

        return new Promise(async resolve => {
          events.on('done', resolve)
          await sleep(250)
          await node3.publish(topic, message, {partDelay: 100})
        })
      })

      it('testing message size limit - very big object', async function () {
        const message = {
          a: genStringByLength(10*1000*1000, 'a'),
          b: genStringByLength(10*1000*1000, 'b'),
          c: genStringByLength(10*1000*1000, 'c'),
        }

        const topic = 'veryBIGobject'

        numberOfMessages = 0
        expectedMessages = 2

        await node1.subscribe(topic, handleMessageEvent(message).bind(node1))
        let topics = node1._pubsub.ls()
        assert(topics.includes(topic))

        await node2.subscribe(topic, handleMessageEvent(message).bind(node2))
        topics = node2._pubsub.ls()
        assert(topics.includes(topic))

        await node3.subscribe(topic, handleMessageEvent(message).bind(node3))
        topics = node3._pubsub.ls()
        assert(topics.includes(topic))

        return new Promise(async resolve => {
          events.on('done', resolve)
          await sleep(250)
          await node3.publish(topic, message, {partDelay: 50})
        })
      })
    })
  })
})
