/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const Identities = require('orbit-db-identity-provider')
const DBInterfACE = require('./db-interface')
const Blockchain = require('./blockchain')

const Logger = require('logplease')
const logger = Logger.create('ace-node', { color: Logger.Colors.Magenta })
Logger.setLogLevel('ERROR')

/**
 * Represents an ACE node, which interacts with the several databases belonging to a single node.
 */
class ACENode {
  /**
   * @param {IPFS} ipfs The IPFS API object.
   * @param {Object} options Manual options.
   * @returns {ACENode} The ACENode object.
   */
  constructor (ipfs, options = {}) {
    logger.debug(`Instantiating ACE node`)
    if (!ipfs)
      throw new Error('ACENode: Missing ipfs argument. IPFS instance must be provided')
    this.ipfs = ipfs
    this.id = options.id

    this.blockchain = options.blockchain || new Blockchain(options.blockchainUrl, options.blockchainPublicKeyType)
    logger.debug(`Blockchain instantiated - URL: ${this.blockchain.url} | TYPE: ${this.blockchain.publicKeyType}`)
    this.keyUpdatePeriod = options.keyUpdatePeriod

    this.dbs = {}
    this.options = options
  }

  /**
   * Initializes the ACENode class. Method is necessary as some methods are asynchronous.
   * Sets up the node's ID and registers it on the blockchain. Method only executes once
   * per node - aborts if already invoked before.
   * @param {Object} options Manual options.
   */
  async init (options = {}) {
    if (this.initDone) return // makes sure initialization process executes only once
    this.initDone = true

    logger.debug(`init: initializing ACE node...`)
    if (!this.id) {
      this.id = options.id || (this.ipfs.peerId ? this.ipfs.peerId.id : (await this.ipfs.id()).id)
    }

    const identityOptions = Object.assign({},
                                          { identityKeysPath: `orbitdb/identity-keys-path/${this.id}` },
                                          options.identityOptions)

    this.identity = options.identity || await Identities.createIdentity({
                                                id: this.id,
                                                ...identityOptions
                                              })

    if (options.blockchain || options.blockchainUrl || options.blockchainPublicKeyType) {
      // if blockchain options are given, reset blockchain instance
      this.blockchain = options.blockchain || new Blockchain(options.blockchainUrl, options.blockchainPublicKeyType)
    }

    await this.registerNodeBlockchain()
    if (this.keyUpdatePeriod) {
      const updater = async () => await this.updatePublicKeys()
      this.keyUpdateIntervalObj = setInterval(updater, this.keyUpdatePeriod)
    }

    logger.debug(`init: node successfully initialized`)
  }

  /**
   * Close method to close the databases' interfaces.
   */
  close () {
    Object.values(this.dbs).forEach((dbI) => dbI.close())
  }

  /**
   * Adds a database to be managed by the node. Invokes the `init` method.
   * @param {DocumentStore} db The database to be added.
   * @param {Object} options Manual options.
   * @returns {DBInterfACE} The corresponding DBInterfACE object
   */
  async addDB (db, options) {
    logger.debug(`addDB: adding database '${db.address.toString()}' to ACE node`)
    await this.init(options) // triggers the init process in case this is the first DB being added

    if (this.dbs[db.address.toString()]) {
      logger.error(`addDB: database with address ${db.address.toString()} was already previously added to this ACE node`)
    } else {
      const opts = Object.assign({}, this.options, options)
      this.dbs[db.address.toString()] = await DBInterfACE.createDBInterface(this.id, this.ipfs, db, this.identity, opts)
    }

    logger.debug(`addDB: database '${db.address.toString()}' successfully added to ACE node`)
    return this.dbs[db.address.toString()]
  }

  /**
   * Retrieves a database's ACE interface given its OrbitDB address.
   * @param {string} dbAddress The database's OrbitDB address.
   * @returns {DBInterfACE} The corresponding DBInterfACE object
   */
  getDBInterface (dbAddress) {
    if (!this.dbs[dbAddress]) {
      logger.error(`getDBInterface: database with address '${dbAddress}' does not exist in this ACE node`)
      return null
    }
    logger.debug(`getDBInterface: getting database interface for '${dbAddress}'`)
    return this.dbs[dbAddress]
  }

  /**
   * Returns the ACE node's public key.
   * @returns {PublicKey} The node's public key.
   */
  get publicKey () {
    return this.identity.publicKey
  }

  /**
   * Adds the <id, public key> pair of a node to the given database's ACE interface.
   * @param {string} dbAddress The database's OrbitDB address.
   * @param {string} id The ID of the node whose info is being added.
   * @param {PublicKey} key The public key of the node whose info is being added.
   */
  addKeyOneDB (dbAddress, id, key) {
    try {
      this.getDBInterface(dbAddress).addPublicKey(id, key)
    } catch (e) {
      if (e.message === 'PublicKeyStore: You are trying to add a key for an id that already exists')
        logger.warn(e.message)
      else
        throw e
    }
  }

  /**
   * Adds the <id, public key> pair of a node to all the current node's databases' ACE interfaces.
   * @param {string} id The ID of the node whose info is being added.
   * @param {PublicKey} key The public key of the node whose info is being added.
   */
  addKeyAllDBs (id, key) {
    for (const dbAddress of Object.keys(this.dbs)) {
      this.addKeyOneDB(dbAddress, id, key)
    }
  }

  /**
   * Registers the node's info (id and public key) to the blockchain, if the info is not there yet.
   */
  async registerNodeBlockchain () {
    logger.debug('registerNodeBlockchain: sending node public info to blockchain')
    if (await this.blockchain.isKeyInBlockchain(this.id, this.publicKey)) {
      logger.debug('registerNodeBlockchain: public key already present in blockchain')
      return
    }

    await this.blockchain.postPublicKey(this.id, this.publicKey)
    logger.debug('registerNodeBlockchain: id and public key successfully registered in blockchain')
  }

  /**
   * Retrieves all keys and IDs from the blockchain and adds them to all of the local node's db
   * ACE interfaces.
   */
  async updatePublicKeys () {
    logger.debug('updatePublicKeys: retrieving keys from blockchain')
    const keys = await this.blockchain.getUniquePublicKeys()

    logger.debug('updatePublicKeys: adding keys to local public key store')
    for (const k of keys) {
      this.addKeyAllDBs(k.id, k.publicKey)
    }

    logger.debug('updatePublicKeys: finished adding keys')
  }
}

module.exports = ACENode
