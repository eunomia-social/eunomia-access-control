/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const Loki = require('lokijs')
const crypto = require('crypto')

class LokiDB {
  constructor (db) {
    this.db = db
  }

  static async open (dbname, options = {}) {
    const dbnameHashed = crypto.createHash('sha256').update(dbname, 'utf8').digest('hex')
    return new Promise((resolve) => {
      const init = (db) => resolve(new LokiDB(db))

      const db = new Loki(dbnameHashed, {
        autoload: true,
        autoloadCallback : () => init(db),
        autosave: options.autosave === undefined ? true : options.autosave,
        autosaveInterval: options.autosaveInterval || 5000
      })
    })
  }

  openCollection (collectionName, key='id') {
    let collection = this.db.getCollection(collectionName);
    if (collection === null) {
      collection = this.db.addCollection(collectionName, { unique: [key] });
    }
    return new LokiCollection(this, collection, key)
  }

  close () {
    this.db.close()
  }
}

class LokiCollection {
  constructor (db, collection, key) {
    this.db = db
    this.collection = collection
    this.key = key
  }

  get (key) {
    return (this.collection.by(this.key, key) || {}).data
  }

  set (key, value) {
    const obj = this.collection.by(this.key, key)
    if (obj) {
      obj.data = value
      this.collection.update(obj)
    } else {
      const insertObj = {}
      insertObj[this.key] = key
      insertObj.data = value
      this.collection.insert(insertObj)
    }
  }

  del (key) {
    this.collection.chain().find({ [this.key]: key }).remove()
  }

  clear () {
    this.collection.clear()
  }
}

module.exports = { LokiDB, LokiCollection }
