/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const axios = require('axios')

const PUBLIC_KEY_TYPE = 'PUBLIC-KEY'
const DEFAULT_ID = 100
const DEFAULT_URL = 'http://87.228.184.201:9091/api'

class Blockchain {
  constructor (url, publicKeyType) {
    this.url = url || DEFAULT_URL
    this.publicKeyType = publicKeyType || PUBLIC_KEY_TYPE
  }

  async retrieveKeyHistory () {
    const res = await axios.get(this.url + `/entries_history/${this.publicKeyType}`)
    return res.data
  }

  async getPublicKeys () {
    const fullHistory = await this.retrieveKeyHistory()
    return fullHistory.reduce((acc, e) => acc.concat(e.history), [])
                   .map(e => ({ id: e.Value.ipfs, publicKey: e.Value.signature }))
  }

  async getUniquePublicKeys () {
    const fullHistory = await this.retrieveKeyHistory()
    const uniques = {}

    fullHistory.reduce((acc, e) => acc.concat(e.history), [])
               .forEach(e => {
                    if (e.Value && e.Value.ipfs && e.Value.signature)
                      uniques[e.Value.ipfs] = { id: e.Value.ipfs, publicKey: e.Value.signature }
                  })

    return Object.values(uniques)
  }

  async isKeyInBlockchain (id, publicKey) {
    const keys = await this.getUniquePublicKeys()

    for (const k of keys) {
      if (k.id === id && k.publicKey === publicKey) return true
    }

    return false
  }

  async postPublicKey (id, publicKey) {
    const obj = {
      id: DEFAULT_ID,
      timestamp: 0,
      ipfs: id,
      signature: publicKey,
      type: this.publicKeyType
    }

    const res = await axios.post(this.url + '/entries', obj)
    return res.data
  }
}

module.exports = Blockchain
