/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

// based off https://github.com/orbitdb/orbit-db-pubsub

const crypto = require('crypto')

const Logger = require('logplease')
const logger = Logger.create('ace-pubsub', { color: Logger.Colors.Grey })
Logger.setLogLevel('ERROR')

const DEFAULT_MAX_MESSAGE_BYTES = 4*1000*1000 // approx 4MB
const maxTopicsOpen = 256
let topicsOpenCount = 0

const serialize = (message) => Buffer.from(JSON.stringify(message))
const deserialize = (message) => JSON.parse(message)
const hash = (message) => crypto.createHash('md5').update(message, 'utf-8').digest().toString('hex')
const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

class PubSub {
  constructor (ipfs, id, options={}) {
    this._ipfs = ipfs
    this._pubsub = ipfs.pubsub

    this.id = id || ipfs.peerId.id
    this._subsCallbacks = {}
    this._multipart = {}
    this.maxMessageBytes = options.maxMessageBytes || DEFAULT_MAX_MESSAGE_BYTES
    this.partDelay = options.partDelay
    this.options = options

    if (this._pubsub === null)
      logger.error("The provided version of ipfs doesn't have pubsub support. Messages will not be exchanged.")

    this._handleMessage = this._handleMessage.bind(this)

    // Bump up the number of listeners we can have open
    if (this._ipfs.setMaxListeners)
      this._ipfs.setMaxListeners(maxTopicsOpen)
  }

  static async createPubSubInstance (ipfs, options={}) {
    const id = ipfs.peerId ? ipfs.peerId.id : (await ipfs.id()).id
    return new PubSub(ipfs, id, options)
  }

  async subscribe(topic, onMessageCallback, options = {}) {
    if(this._subsCallbacks[topic]) {
      logger.debug(`subscribe: Topic ${topic} already subscribed. Doing nothing.`)
      return
    }

    await this._pubsub.subscribe(topic, this._handleMessage, options)

    this._subsCallbacks[topic] = onMessageCallback

    topicsOpenCount ++
    logger.debug(`Topics open: ${topicsOpenCount}`)
  }

  async unsubscribe(topic) {
    if(this._subsCallbacks[topic]) {
      await this._pubsub.unsubscribe(topic, this._handleMessage)
      delete this._subsCallbacks[topic]

      logger.debug(`Unsubscribed from '${topic}'`)
      topicsOpenCount --
      logger.debug(`Topics open: ${topicsOpenCount}`)
    }
  }

  async publish(topic, message, options = {}) {
    if(!this._subsCallbacks[topic]) {
      logger.warn(`_publish: not subscribed to ${topic}. You probably want to subscribe topic before publishing.`)
    }

    const messageBytes = serialize(message)
    logger.debug(`publish: message size: ${messageBytes.length} Bytes ` +
                      `(${(messageBytes.length/1024).toFixed(2)}KB, ` +
                      `${(messageBytes.length/(1024*1024)).toFixed(2)}MB)`)

    if (messageBytes.length > this.maxMessageBytes) {
      const messageStr = JSON.stringify(message)

      const numberOfParts = Math.ceil(messageStr.length / this.maxMessageBytes)
      const msgHash = hash(messageStr)
      logger.debug(`publish: splitting '${msgHash}' in ${numberOfParts} parts`)

      for (let i=0; i<numberOfParts; i++) {
        const partObj = {
          MULTIPART: true,
          hash: msgHash,
          numberOfParts,
          seqno: i,
          part: messageStr.slice(i*this.maxMessageBytes,(i+1)*this.maxMessageBytes)
        }
        this._pubsub.publish(topic, serialize(partObj), options)
        if (options.partDelay || this.partDelay) await sleep(options.partDelay || this.partDelay)
      }

      logger.debug(`publish: sent all parts`)
      return
    }

    this._pubsub.publish(topic, messageBytes, options)
  }

  async disconnect() {
    for (const topic of Object.keys(this._subsCallbacks))
      await this.unsubscribe(topic)
    this._subsCallbacks = {}
  }

  async _handleMessage(message) {
    // Don't process our own messages
    if (message.from === this.id)
      return

    // Get the message content and a subscription
    let content, onMessage, topicId
    try {
      // Get the topic
      topicId = message.topicIDs[0]
      content = deserialize(message.data)
      onMessage = this._subsCallbacks[topicId]
    } catch (e) {
      logger.error(e)
      logger.error('Couldn\'t parse pubsub message:', message)
    }

    if (onMessage && content && content.MULTIPART) {
      logger.debug(`MULTIPART message received. ID: '${content.hash}', seqno: ${content.seqno}`)

      if (!this._multipart[content.hash]) {
        this._multipart[content.hash] = {
          parts: [],
          numberOfParts: content.numberOfParts
        }
      }

      if (!this._multipart[content.hash].parts[content.seqno]) {
        this._multipart[content.hash].parts[content.seqno] = content.part
      }

      if (this._multipart[content.hash].parts.length === this._multipart[content.hash].numberOfParts
            && !this._multipart[content.hash].parts.includes(undefined)) {
        logger.debug(`All ${this._multipart[content.hash].numberOfParts} parts of '${content.hash}' received.`)
        await onMessage(topicId, deserialize(this._multipart[content.hash].parts.join('')), message.from)
        delete this._multipart[content.hash]
      }
    }

    else if(onMessage && content) {
      await onMessage(topicId, content, message.from)
    }
  }
}

module.exports = PubSub
