/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const jsonDeterministicStringify = require('json-stringify-deterministic')
const EventEmitter = require('events')
const cryptoUtils = require('./crypto-utils')
const PubSub = require('./pubsub')
const LokiDB = require('./loki-controller').LokiDB

const Logger = require('logplease')
const logger = Logger.create('ace-db-interface', { color: Logger.Colors.Magenta })
Logger.setLogLevel('ERROR')

// Default time in milliseconds after which a key's retrieval is aborted
const DEFAULT_KEY_RETRIEVAL_TIMEOUT = 30000

/**
 * Handles the access control checks and operations before executing the operations on the actual
 * OrbitDB database.
 */
class DBInterfACE {
  // TODOs:
  // - encrypt RESPONSE-SHAREs with requester public key before sending it back
  // - write tests with fake/failing signatures for STORE-SHARE and REQUEST-SHARE requests
  // - write tests with repeating nonces for STORE-SHARE and REQUEST-SHARE requests

  /**
   * @param {string} id The ACE node's ID.
   * @param {IPFS} ipfs The IPFS API object.
   * @param {DocumentStore} db The OrbitDB database object.
   * @param {Identity} identity `orbit-db-identity-provider` object.
   * @param {Object} options Manual options.
   * @returns {DBInterfACE} The DBInterfACE object
   */
  constructor (id, ipfs, db, identity, options = {}) {
    this.id = id
    this.ipfs = ipfs
    this.db = db
    this.indexBy = this.db.options.indexBy
    this.identity = identity

    this.keyRetrievalTimeout = options.keyRetrievalTimeout || DEFAULT_KEY_RETRIEVAL_TIMEOUT
    this.events = new EventEmitter()

    if (options.secretSharingThreshold) {
      this.threshold = Number(options.secretSharingThreshold)
      logger.info(`DBInterfACE: global secret sharing threshold set to ${this.threshold}`)
    }

    this.receivedShares = {}
    this.nonces = {}

    this.addPublicKey(this.id, this.publicKey)
  }

  /**
   * Creates and initializes the given database's ACE interface. Requires async method invocations.
   * @param {string} id ACE node's ID.
   * @param {IPFS} ipfs IPFS API object.
   * @param {DocumentStore} db OrbitDB database object.
   * @param {Identity} identity `orbit-db-identity-provider` object.
   * @param {Object} options Manual options.
   * @returns {DBInterfACE} The corresponding DBInterfACE object
   */
  static async createDBInterface (id, ipfs, db, identity, options = {}) {
    logger.debug(`createDBInterface: instantiating DB interface - ID: ${id} | DB id: ${db.address.toString()}`)
    const dbInterface = new DBInterfACE(id, ipfs, db, identity, options)

    dbInterface.loki = await LokiDB.open(`dbInterface-${id}-${db.address.toString()}.db`, options.loki)
    dbInterface.keys = dbInterface.loki.openCollection('keys')
    dbInterface.shares = dbInterface.loki.openCollection('shares')

    dbInterface.pubsub = await PubSub.createPubSubInstance(ipfs, options)
    // Subscribe own topic for direct communications
    await dbInterface.pubsub.subscribe(dbInterface.pubsubTopic(dbInterface.pubsub.id), dbInterface._handleOwnTopic.bind(dbInterface))
    await dbInterface.pubsub.subscribe(dbInterface.pubsubTopic('broadcast'), dbInterface._handleBroadcast.bind(dbInterface))

    logger.debug(`createDBInterface: DB interface successfully instantiated - ID: ${id} | DB id: ${db.address.toString()}`)
    return dbInterface
  }

  /**
   * Retrieves an object from the OrbitDB database. If the object is encrypted, it attempts to
   * decrypt it before returning.
   * @param {string} objectID The identifier of the object.
   * @returns {Object} The requested object.
   */
  async get (objectID) {
    const objects = await this.db.getExactKey(objectID)
    const result = []

    for (const obj of objects) {
      if (!obj.encrypted) {
        result.push(obj.data)
        continue
      }

      try {
        result.push(await this._decryptObj(objectID, obj.data))
      } catch (e) {
        logger.error(`get: ${e}`)
        continue
      }
    }

    return result
  }

  /**
   * Executes a query over all the objects the node has access to. It first retrieves and attempts
   * to decrypt all the objects in the database, then it executes the query over the successfully
   * decrypted objects and the non encrypted objects.
   * @param {Function} mapper The query function. The function is applied to every element, if it
   * returns `true` the object is returned, otherwise the object is not returned.
   * @returns {[Object]} Array of the objects that satisfy the `mapper` condition.
   */
  async queryAll (mapper) {
    const objects = await Object.keys(this.db._index._index)
      .map((e) => [e, this.db._index.get(e, false)])

    let result = []
    const toDecrypt = []

    for (const [id, obj] of objects) {
      if (!obj.encrypted) {
        result.push(obj.data)
      }

      else if (!this.keys.get(id)) {
        toDecrypt.push({id, obj})
      }

      else {
        result.push(await this._decryptObj(id, obj.data))
      }
    }

    if (toDecrypt.length > 0)
      result = result.concat(await this._batchDecrypt(toDecrypt))

    return result.filter(mapper)
  }

  /**
   * Executes a query over all the objects in the database, returning all that match the query
   * before decryption. Only unencrypted or successfully decrypted objects are returned. First the
   * query is executed over the database as it is, then the results are decrypted in case they are
   * encrypted. As the query is executed over the database before decryption, when the query is
   * over encrypted data, this might yield unexpected results.
   *
   * NOTE: All data inside the `cleartextProperties` field will remain unencrypted; thus, you might
   * want to use this field to take full advantage of this method and speed up searches over
   * encrypted data.
   *
   * @param {Function} mapper The query function. The function is applied to every element, if it
   * returns `true` the object is returned, otherwise the object is not returned.
   * @returns {[Object]} Array of the objects that satisfy the `mapper` condition.
   */
  async query (mapper) {
    const objects = await this.db.query(mapper)

    let result = []
    const toDecrypt = []

    for (const obj of objects) {
      if (!obj.encrypted) {
        result.push(obj.data)
      }

      else if (!this.keys.get(obj[this.indexBy])) {
        toDecrypt.push({id: obj[this.indexBy], obj})
      }

      else {
        result.push(await this._decryptObj(obj[this.indexBy], obj.data))
      }
    }

    if (toDecrypt.length > 0)
      result = result.concat(await this._batchDecrypt(toDecrypt))

    return result
  }

  /**
   * Object create operation. When read permissions are to be enforced over the object being
   * created, this method generates the encryption key, distributes it, and encrypts the object
   * before storing it in the OrbitDB database.
   * @param {Object} obj The object being created.
   * @param {Number} totalShares (optional) Number of shares to split the encryption key into.
   * @param {Number} threshold (optional) Number of shares necessary to reconstruct the key.
   * @returns {Object} The OrbitDB operation receipt.
   */
  async create (obj, totalShares, threshold) {
    if (!obj.permissions || !obj.permissions.read) // no read permissions set => no encryption
      return await this.db.create((await this._wrap(obj)))

    let key = cryptoUtils.keygen(cryptoUtils.randomString(64)) // generate 64-character password
    this.keys.set(obj[this.indexBy], key)

    // Splits key into shares and sends them to other ACE nodes
    await this._distributeKeyShares(obj, key, { totalShares, threshold })

    const encryptedObj = cryptoUtils.encrypt(key, JSON.stringify(obj))

    return await this.db.create((await this._wrap(obj, encryptedObj)))
  }

  /**
   * Object update operation. If read permissions are enforced, object is either encrypted with the
   * existing encryption key, or, if read permissions are being changed by the object's owner, a
   * new encryption key is generated and distributed, and then the object is encrypted with it.
   * Finally, the encrypted object is stored in the OrbitDB database.
   * @param {Object} obj The updated object.
   * @param {Number} totalShares (optional) Number of shares to split the new encryption key into.
   * @param {Number} threshold (optional) Number of shares necessary to reconstruct the key.
   * @returns {Object} The OrbitDB operation receipt.
   */
  async update (obj, totalShares, threshold) {
    if (obj.encrypted === false) return await this.db.update((await this._wrap(obj)))

    const changingPermissions = (obj) => {
      if (!obj.permissions || !obj.permissions.read) return false

      const shareObj = this.shares.get(obj[this.indexBy])
      if (!shareObj) {
        logger.warn(`update: no share present for this object. Changing permissions not allowed`)
        return false
      }

      if (shareObj.owner !== this.id) return false // only owner can change permissions

      return jsonDeterministicStringify(shareObj.readPermissions)
                !== jsonDeterministicStringify(obj.permissions.read)
    }

    if (changingPermissions(obj)) {
      let key = cryptoUtils.keygen(cryptoUtils.randomString(64)) // generate new key
      this.keys.set(obj[this.indexBy], key)

      // Splits key into shares and sends them to other ACE nodes
      await this._distributeKeyShares(obj, key, {update:true, totalShares, threshold})
    }

    if (!this.keys.get(obj[this.indexBy])) {
      // TODO retrieve key if not present
      logger.warn(`update: key needed for object '${obj[this.indexBy]}' not present locally. Storing object in plaintext.`)
      return await this.db.update((await this._wrap(obj)))
    }

    const encryptedObj = cryptoUtils.encrypt(this.keys.get(obj[this.indexBy]), JSON.stringify(obj))

    return await this.db.update(await this._wrap(obj, encryptedObj))
  }

  /**
   * Object delete operation.
   * @param {string} id The identifier of the object to be deleted.
   * @returns {Object} The OrbitDB operation receipt.
   */
  async del (id) {
    const requestInfo = {
      value: {
        entityID: this.id,
        nonce: cryptoUtils.randomString(24)
      }
    }
    requestInfo.value[this.db.options.indexBy] = id
    requestInfo.value.signature = await this._signObject(requestInfo.value)

    return await this.db.del(id, requestInfo)
  }

  /**
   * Closes the loki database used to persist object decryption keys and key shares.
   */
  close () {
    // Close loki database
    this.loki.close()
  }

  /**
   * Clears object keys and key shares. For testing purposes.
   */
  clearKeys () {
    this.receivedShares = {}
    this.keys.clear()
  }

  /**
   * Returns the ACE node's public key.
   * @returns {PublicKey} ACE node's public key.
   */
  get publicKey () {
    return this.identity.publicKey
  }

  /**
   * Given an entity's identifier, retrieves a public key from the database level access control
   * key store.
   * @param {string} id The entity's identifier.
   * @returns {PublicKey} The entity's public key.
   */
  getPublicKey (id) {
    return this.db.getKey(id)
  }

  /**
   * Adds an <id, public key> pair to the database level access control key store.
   * @param {string} id The entity's identifier.
   * @param {PublicKey} key The entity's public key.
   */
  addPublicKey (id, key) {
    this.db.addKey(id, key)
  }

  /**
   * Generates a string derived from a given string and database address, to be used as a
   * pubsub topic.
   * @param {string} topic Topic name.
   * @param {string} dbAddress Database's OrbitDB address.
   * @returns {string} A pubsub topic name.
   */
  pubsubTopic (topic, dbAddress = null) {
    return topic + ':' + (dbAddress || this.db.address.toString())
  }

  /**
   * Pubsub message handler for the node's own topic (associated with the database being
   * interfaced). It handles requests for storing key shares, and for setting and updating objects'
   * permissions. Also handles the responses from other ACE nodes to key share requests.
   * @param {string} topic Topic name.
   * @param {Object} msg Pubsub message object.
   * @param {string} from IPFS identifier of the message's sender.
   */
  async _handleOwnTopic (topic, msg, from) {
    logger.debug(`_handleOwnTopic: received message from ${from}`)

    const entity = (msg.op === 'STORE-SHARE' || msg.op === 'UPDATE-SHARE') ? msg.owner : msg.entityID
    if (!(await this._verifySignature(msg, entity))) {
      logger.error(`_handleOwnTopic: ${msg.op} request has an invalid signature. Ignoring request...`)
      return
    }

    if (msg.op === 'STORE-SHARE') {
      logger.debug(`_handleOwnTopic: storing incoming share`)
      this.shares.set(msg.objectID, msg)
      this._addReceivedShare(msg)
    }

    else if (msg.op === 'UPDATE-SHARE') {
      logger.debug(`_handleOwnTopic: updating existing share and object's read permissions`)

      const storedShare = this.shares.get(msg.objectID)
      if (storedShare && storedShare.owner !== entity) {
        logger.debug(`_handleOwnTopic: only object owner can change read permissions`)
        return
      }

      this.shares.set(msg.objectID, msg) // overwrite existing share object

      if (this.keys.get(msg.objectID)) {
        // delete the previously reconstructed key as it is now outdated
        this.keys.del(msg.objectID)
      }

      delete this.receivedShares[msg.objectID]
      this._addReceivedShare(msg)
    }

    // TODO think about DoS
    else if (msg.op === 'RESPONSE-SHARE') {
      logger.debug(`_handleOwnTopic: response share received`)
      this._addReceivedShare(msg)
    }

    else if (msg.op === 'UNAUTHORIZED') {
      logger.debug(`_handleOwnTopic: ${from} says you cannot read object ${msg.objectID}`)
      this.events.emit(`unauthorized-${msg.objectID}`, msg)
    }

    else if (msg.op === 'BATCH-RESPONSE-SHARE') {
      logger.debug(`_handleOwnTopic: batch response shares received`)
      for (const share of msg.responseShares) {
        if (share.status === 'OK')
          this._addReceivedShare(share)
        else if (share.status === 'UNAUTHORIZED')
          this.events.emit(`unauthorized-${share.objectID}`, msg)
      }
    }
  }

  /**
   * Pubsub message handler for the broadcast topic (associated with the interface's database).
   * It handles requests for key shares.
   * @param {string} topic Topic name.
   * @param {Object} msg Pubsub message object.
   * @param {string} from IPFS identifier of the message's sender.
   */
  async _handleBroadcast (topic, msg, from) {
    logger.debug(`_handleBroadcast: received message from ${from}: ${JSON.stringify(msg)}`)

    const hasPermission = (shareObj, requester) => {
      if (!shareObj.readPermissions) return true // no permissions set so anyone is allowed to read
      if (shareObj.readPermissions.includes('*')) return true // all can read
      return shareObj.readPermissions.includes(requester) || shareObj.owner === requester
    }

    if (!(await this._verifySignature(msg, msg.entityID))) {
      logger.error(`_handleBroadcast: ${msg.op} request has an invalid signature. Ignoring request...`)
      return
    }

    if (msg.op === 'REQUEST-SHARE') {
      // send requested share back to sender
      logger.debug(`_handleBroadcast: key share requested for object '${msg.objectID}'`)

      // subscribe the requester's topic to start relaying messages from this topic
      await this.pubsub.subscribe(this.pubsubTopic(from), (() => {}))

      const storedShare = this.shares.get(msg.objectID)

      if (!storedShare) {
        logger.debug(`_handleBroadcast: node does not have a share for this object`)
        return
      }

      if (!hasPermission(storedShare, msg.entityID)) {
        logger.warn(`_handleBroadcast: requester does not have permission to read object '${msg.objectID}'`)

        const response = {
          op: 'UNAUTHORIZED',
          entityID: this.id,
          objectID: msg.objectID,
          nonce: cryptoUtils.randomString(24)
        }
        response.signature = await this._sign(jsonDeterministicStringify(response))
        await this.pubsub.publish(this.pubsubTopic(from), response)

        return
      }

      const response = {
        op: 'RESPONSE-SHARE',
        entityID: this.id,
        objectID: storedShare.objectID,
        share: storedShare.share,
        threshold: storedShare.threshold,
        nonce: cryptoUtils.randomString(24)
      }
      response.signature = await this._sign(jsonDeterministicStringify(response))
      await this.pubsub.publish(this.pubsubTopic(from), response)
      logger.debug(`_handleBroadcast: key share sent`)
    }

    else if (msg.op === 'BATCH-REQUEST-SHARE') {
      // send requested shares back to sender
      logger.debug(`_handleBroadcast: [batch request] ${msg.IDs.length} key shares requested`)

      // subscribe the requester's topic to start relaying messages from this topic
      await this.pubsub.subscribe(this.pubsubTopic(from), (() => {}))

      const responseShares = []
      for (const objectID of msg.IDs) {
        const storedShare = this.shares.get(objectID)

        if (!storedShare) continue

        if (!hasPermission(storedShare, msg.entityID)) {
          responseShares.push({
            objectID: objectID,
            status: 'UNAUTHORIZED'
          })
          continue
        }

        responseShares.push({
          objectID: storedShare.objectID,
          status: 'OK',
          share: storedShare.share,
          threshold: storedShare.threshold
        })
      }

      const response = {
        op: 'BATCH-RESPONSE-SHARE',
        entityID: this.id,
        responseShares: responseShares,
        nonce: cryptoUtils.randomString(24)
      }
      response.signature = await this._sign(jsonDeterministicStringify(response))

      await this.pubsub.publish(this.pubsubTopic(from), response)

      logger.debug(`_handleBroadcast: [batch request] ${responseShares.length} key shares sent`)
    }
  }

  /**
   * Signs the given data with the ACE node's key, using ECDSA over secp256k1.
   * @param {string} data The data to be signed.
   * @returns {Signature} The ECDSA signature.
   */
  async _sign (data) {
    // ECDSA signature using secp256k1
    return await this.identity.provider.sign(this.identity, data)
  }

  /**
   * Verifies if an object contains a signature, if it is valid, and if generated by the given
   * entity.
   * @param {Object} req The object which signature is being verified.
   * @param {string} entity The entity's identifier.
   * @returns {boolen} `true` if signature is valid, `false` otherwise.
   */
  async _verifySignature (req, entity) {
    if (!req.signature) return false
    if (!req.nonce) return false

    const publicKey = this.getPublicKey(entity)
    if (!publicKey) return false

    const toSign = jsonDeterministicStringify({
      op: req.op,
      objectID: req.objectID,
      entityID: req.entityID,
      owner: req.owner,
      readPermissions: req.readPermissions,
      share: req.share,
      IDs: req.IDs,
      responseShares: req.responseShares,
      threshold: req.threshold,
      nonce: req.nonce
    })

    const sigValid = await this.identity.provider.verify(req.signature, publicKey, toSign)

    if (sigValid && !this.nonces[req.nonce]) {
      this.nonces[req.nonce] = true
      return true
    } else {
      return false
    }
  }

  /**
   * Manages and stores received shares in local data structures.
   * @param {Object} msg Key share object.
   */
  _addReceivedShare (msg) {
    if (!this.receivedShares[msg.objectID]) {
      this.receivedShares[msg.objectID] = {
        objectID: msg.objectID,
        threshold: msg.threshold,
        shares: [ msg.share ]
      }
      this.events.emit(`new-share-${msg.objectID}`, this.receivedShares[msg.objectID])
    } else if (!this.receivedShares[msg.objectID].shares.includes(msg.share)) {
      this.receivedShares[msg.objectID].shares.push(msg.share)
      this.events.emit(`new-share-${msg.objectID}`, this.receivedShares[msg.objectID])
    }
  }

  /**
   * Handles the retrieval of the decryption for the object with the given ID. This means
   * requesting the necessary shares from other ACE nodes and reconstructing the key once enough
   * shares are received.
   * @param {string} objectID The identifier of the object.
   * @returns {Key} The object's decryption key. Throws exception if retrieval fails.
   */
  async _retrieveKey (objectID) {
    logger.debug(`_retrieveKey: initializing key retrieval`)

    const getRequestObj = async (objectID) => {
      const reqObj = {
        op: 'REQUEST-SHARE',
        objectID: objectID,
        entityID: this.id,
        nonce: cryptoUtils.randomString(24)
      }
      reqObj.signature = await this._sign(jsonDeterministicStringify(reqObj))

      return reqObj
    }

    const req = await getRequestObj(objectID)

    await this.pubsub.publish(this.pubsubTopic('broadcast'), req)

    const waitForShares = async function (resolve, reject) {
      const timeoutFunc = () => {
        logger.error('_retrieveKey: key retrieval timed out')
        reject('_retrieveKey: timed out')
      }
      const timeout = setTimeout(timeoutFunc, this.keyRetrievalTimeout)

      let unauthorizedResponses = 0
      const updateUnauthorized = (msg) => {
        const threshold = this.threshold || msg.threshold
        unauthorizedResponses++
        if (unauthorizedResponses >= threshold) {
          logger.error(`_retrieveKey: you have received the threshold number (${threshold}) of 'unauthorized'`
                       + ` responses for '${objectID}'. Key retrieval failed`)

          clearTimeout(timeout)
          this.events.removeListener(`unauthorized-${objectID}`, updateUnauthorized)
          this.events.removeListener(`new-share-${objectID}`, checkProgress)
          reject('_retrieveKey: no read permission')
        }
      }
      this.events.on(`unauthorized-${objectID}`, updateUnauthorized)

      const checkProgress = (shares) => {
        logger.debug(`_retrieveKey: currently have ${shares.shares.length} share(s)`)
        const threshold = this.threshold || shares.threshold
        if (shares.shares.length >= threshold) {
          const key = cryptoUtils.deserializeKey(cryptoUtils.combine(shares.shares))
          logger.debug(`_retrieveKey: key successfully reassembled`)

          clearTimeout(timeout)
          this.events.removeListener(`unauthorized-${objectID}`, updateUnauthorized)
          this.events.removeListener(`new-share-${objectID}`, checkProgress)
          resolve(key)
        }
      }
      this.events.on(`new-share-${objectID}`, checkProgress)
    }

    return new Promise(waitForShares.bind(this))
  }

  /**
   * Attempts to retrieve an object's decryption keys from the local store. If not present locally,
   * the retrieval process is triggered.
   * @param {string} objectID The identifier of the object.
   * @returns {Key} The object's decryption key.
   */
  async _getKey (objectID) {
    let key = this.keys.get(objectID)
    if (!key) {
      key = await this._retrieveKey(objectID)
      this.keys.set(objectID, key)
    }
    return key
  }

  /**
   * Attempts to decrypt an encrypted object given its ID. The decryption key is obtained by
   * invoking `_getKey`.
   * @param {string} objectID The identifier of the object.
   * @param {string} encryptedObj The data consisting of the encrypted object.
   * @returns {Object} The decrypted object.
   */
  async _decryptObj (objectID, encryptedObj) {
    let key

    try {
      key = await this._getKey(objectID)
    } catch (e) {
      logger.error(`decryptObj: key retrieval failed for object '${objectID}'`)
      throw e
    }

    return JSON.parse(cryptoUtils.decrypt(key, encryptedObj))
  }

  /**
   * Given an array of object identifiers, it attempts to retrieve the corresponding decryption
   * keys using a batch request. The keys are then stored in the local data structures. This method
   * improves performance over `_retrieveKey`. It avoids the network overhead of making a unique
   * request for every single key.
   * @param {[string]} IDs The array of object IDs.
   */
  async _batchRetrieveKeys (IDs) {
    const waitForShares = objectID => async function (resolve, reject) {
      const timeoutFunc = () => {
        logger.error('_batchRetrieveKeys: key retrieval timed out')
        resolve()
      }
      const timeout = setTimeout(timeoutFunc, this.keyRetrievalTimeout)

      let unauthorizedResponses = 0
      const updateUnauthorized = (msg) => {
        const threshold = this.threshold || msg.threshold
        unauthorizedResponses++
        if (unauthorizedResponses >= threshold) {
          logger.error(`_batchRetrieveKeys: you have received the threshold number (${threshold}) of 'unauthorized'`
                       + ` responses for '${objectID}'. Key retrieval failed`)

          clearTimeout(timeout)
          this.events.removeListener(`unauthorized-${objectID}`, updateUnauthorized)
          this.events.removeListener(`new-share-${objectID}`, checkProgress)
          resolve()
        }
      }
      this.events.on(`unauthorized-${objectID}`, updateUnauthorized)

      const checkProgress = (shares) => {
        logger.debug(`_batchRetrieveKeys: currently have ${shares.shares.length} share(s)`)
        const threshold = this.threshold || shares.threshold
        if (shares.shares.length >= threshold) {
          const key = cryptoUtils.deserializeKey(cryptoUtils.combine(shares.shares))
          this.keys.set(objectID, key)
          logger.debug(`_batchRetrieveKeys: key successfully reassembled and stored`)

          clearTimeout(timeout)
          this.events.removeListener(`unauthorized-${objectID}`, updateUnauthorized)
          this.events.removeListener(`new-share-${objectID}`, checkProgress)
          resolve(key)
        }
      }
      this.events.on(`new-share-${objectID}`, checkProgress)
    }

    const sharePromises = IDs.map(id => new Promise(waitForShares(id).bind(this)))

    const reqObj = {
      op: 'BATCH-REQUEST-SHARE',
      IDs: IDs,
      entityID: this.id,
      nonce: cryptoUtils.randomString(24)
    }
    reqObj.signature = await this._sign(jsonDeterministicStringify(reqObj))
    await this.pubsub.publish(this.pubsubTopic('broadcast'), reqObj)

    await Promise.all(sharePromises)
  }

  /**
   * Given an array of encrypted objects, attempts to decrypt them, and returns the decrypted
   * objects. It first attempts to collect all the given object's decryption keys and then proceeds
   * to the decryption. Expects array of objects {id, obj}.
   * @param {[Object]} toDecrypt The array of objects to decrypt.
   * @returns {[Object]} Array of the decrypted objects (the ones for which the key was
   * successfully retrieved).
   */
  async _batchDecrypt (toDecrypt) {
    logger.debug(`batchDecrypt: ${toDecrypt.length} objects to decrypt`)

    await this._batchRetrieveKeys(toDecrypt.map(e => e.id))

    const result = []
    for (const {id, obj} of toDecrypt) {
      // Attempt decryption for existing keys only - so key retrieval is not triggered again
      if (this.keys.get(id)) {
        logger.debug(`batchDecrypt: key for '${id}' available`)
        result.push(await this._decryptObj(id, obj.data))
      }
    }

    return result
  }

  /**
   * Splits the given key into shares and sends them to other ACE nodes.
   * @param {Object} obj The object which the key is meant to encrypt.
   * @param {Key} key The key to be distributed.
   * @param {Object} options Options: {
   *                            totalShares: total number of shares to split key (optional),
   *                            threshold: number of shares necessary to reconstruct key (optional),
   *                            update: set to `true` when operation is an update, leave blank o/w
   *                          }
   */
  async _distributeKeyShares (obj, key, options = {}) {
    // TODO add step to retrieve nodes' public keys, and then
    // broadcast shares but encrypted with each node's key

    const aceNodes = await this.ipfs.pubsub.peers(this.pubsubTopic('broadcast'))

    const totalShares = options.totalShares || aceNodes.length+1 // all connected nodes plus itself
    const threshold = options.threshold || this.threshold || Math.max(Math.ceil(totalShares / 2), 2) // threshold must be >= 2

    // TODO check if threshold > totalShares

    if (totalShares < 2 || threshold < 2) {
      logger.error(`distributeKeyShares: 'totalShares' (${totalShares}) and 'threshold' (${threshold}) must both be integers >= 2.`
                    + ` Key shares will not be sent to other ACE nodes (only stored locally)`)
      return
    }

    const shares = cryptoUtils.split(cryptoUtils.serializeKey(key), totalShares, threshold)
    logger.debug(`distributeKeyShares: split key into ${totalShares} shares, with treshold of ${threshold}`)

    const getShareObject = async (share) => {
      const shareObj = {
        op: options.update ? 'UPDATE-SHARE' : 'STORE-SHARE',
        objectID: obj[this.indexBy],
        owner: this.id,
        readPermissions: obj.permissions ? obj.permissions.read : null,
        share: share,
        threshold,
        nonce: cryptoUtils.randomString(24)
      }
      shareObj.signature = await this._sign(jsonDeterministicStringify(shareObj))
      return shareObj
    }

    // Store first share locally
    const first = shares.shift()
    this.shares.set(obj[this.indexBy], await getShareObject(first))

    for (let i = 0; i < Math.min(shares.length, aceNodes.length); i++) {
      const shareObj = await getShareObject(shares[i])
      await this.pubsub.publish(this.pubsubTopic(aceNodes[i]), shareObj)
      logger.debug(`create: share ${i} sent to ${aceNodes[i].slice(-8)}`)
    }
    logger.debug(`create: Shares successfully sent to ACENodes`)
  }

  /**
   * Signs an object with the ACE node's key. Objects are signed before being stored in the OrbitDB
   * database.
   * @param {Object} obj The object to be signed.
   * @returns {Object} The signature over the given object.
   */
  async _signObject (obj) {
    const toSign = jsonDeterministicStringify({
      objectID: obj[this.indexBy],
      data: obj.data,
      permissions: obj.permissions,
      encrypted: obj.encrypted,
      cleartextProperties: obj.cleartextProperties,
      entityID: obj.entityID,
      nonce: obj.nonce
    })

    return await this._sign(toSign)
  }

  /**
   * Wraps the object to be compatible with the rest of the access control enforcement (the
   * OrbitDB level).
   * @param {Object} obj The object to be wrapped.
   * @param {Object} encryptedObj The encrypted version of the object in case it is to be stored
   * encrypted.
   * @param {boolean} sign (optional) Indicates whether the object should be signed.
   * @returns {Object} The wrapped object.
   */
  async _wrap (obj, encryptedObj, sign = true) {
    const wrappedObj = {}

    wrappedObj[this.indexBy] = obj[this.indexBy]

    if (encryptedObj) {
      wrappedObj.data = encryptedObj
      wrappedObj.encrypted = true
    } else {
      wrappedObj.data = obj
      wrappedObj.encrypted = false
    }

    if (obj.cleartextProperties) {
      wrappedObj.cleartextProperties = obj.cleartextProperties
    }

    if (sign) {
      if (obj.permissions) wrappedObj.permissions = obj.permissions
      wrappedObj.entityID = this.id
      wrappedObj.nonce = cryptoUtils.randomString(24)
      wrappedObj.signature = await this._signObject(wrappedObj)
    }

    return wrappedObj
  }
}

module.exports = DBInterfACE
