/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
'use strict'

const crypto = require('crypto')
const secrets = require('secrets.js-grempe')

// Default values
const ALGORITHM = 'aes-256-ctr'
const KEYLEN = 32 // AES256 => 256bit/32byte key

function randomString (size = 64) {
  return crypto.randomBytes(size).toString('base64')
}

function keygen (password, salt = 'salt', keylen = KEYLEN) {
  salt = salt || 'salt'
  keylen = keylen || KEYLEN

  return crypto.scryptSync(password, 'salt', KEYLEN)
}

function serializeKey (key, encoding = 'hex') {
  return key.toString(encoding)
}

function deserializeKey (keyString, encoding = 'hex') {
  return Buffer.from(keyString, encoding)
}

function encrypt (key, plaintext, options = {}) {
  const algorithm = options.algorithm || ALGORITHM
  const iv = options.iv || crypto.randomBytes(16)

  const cipher = crypto.createCipheriv(algorithm, key, iv)
  const encrypted = Buffer.concat([cipher.update(plaintext), cipher.final()])

  return {
      iv: iv.toString('hex'),
      content: encrypted.toString('hex')
  }
}

function pwEncrypt (password, plaintext, options = {}) {
  const key = keygen(password, options.salt, options.keylen)

  return encrypt(key, plaintext, options)
}

function decrypt (key, ciphertext, options = {}) {
  const algorithm = options.algorithm || ALGORITHM

  const decipher = crypto.createDecipheriv(algorithm, key, Buffer.from(ciphertext.iv, 'hex'));
  const decrpyted = Buffer.concat([decipher.update(Buffer.from(ciphertext.content, 'hex')), decipher.final()]);

  return decrpyted.toString();
}

function pwDecrypt (password, ciphertext, options = {}) {
  let algorithm = options.algorithm || ALGORITHM
  const key = keygen(password, options.salt, options.keylen)

  return decrypt(key, ciphertext, options)
}

function split (secret, total, threshold) {
  return secrets.share(secrets.str2hex(secret), total, threshold)
}

function combine (shares) {
  return secrets.hex2str(secrets.combine(shares))
}

function newShare (id, shares) {
  return secrets.newShare(id, shares)
}

module.exports = {
  randomString,
  keygen,
  serializeKey,
  deserializeKey,
  encrypt,
  decrypt,
  pwEncrypt,
  pwDecrypt,
  split,
  combine,
  newShare
}
