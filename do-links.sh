echo "Linking orbit-db to eunomia-access-control"
npm link ../orbit-db

echo "Switching dir to orbit-db"
cd ../orbit-db
echo "Linking orbit-db-store to orbit-db"
npm link ../orbit-db-store
echo "Linking orbit-db-docstore to orbit-db"
npm link ../orbit-db-docstore

echo "Switching dir to orbit-db-docstore"
cd ../orbit-db-docstore
echo "Linking orbit-db-store to orbit-db-docstore"
npm link ../orbit-db-store

echo "Switching dir back to eunomia-access-control"
cd ../eunomia-access-control
