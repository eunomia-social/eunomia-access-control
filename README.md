# Access Control mechanism for OrbitDB databases

## Installation

After cloning the repository, go into the directory and run
```sh
npm ci
```

You can run the development tests by executing
```sh
npm test
```

## Example Usage
```js
const ACENode = require('eunomia-access-control').ACENode

/*...*/
const aceNode = new ACENode(ipfs, {id: 'node1-ID-abcdef-012345', secretSharingThreshold: 2})

const db = await orbitdbInstance.docs('db-address', options)
const dbInterface = await aceNode.addDB(db)


// ---- Creating an object
const obj = {
  _id: 'object-ID',
  text: 'very important data',
  permissions: {
    read: [ /*IDs allowed to read*/ ],
    write: [ /*IDs allowed to write*/ ],
    delete: [ /*IDs allowed to delete*/ ]
  }
}
await dbInterface.create(obj)


// ---- Updating an object
const obj_updated = {
  _id: 'object-ID', // Make sure it is the correct ID
  text: 'very important but different data',
  newField: 'more data'
  /*no need to add permissions field if not changing them*/
}
await dbInterface.update(obj_updated)


// ---- Making a query
const queryResults = await dbInterface.queryAll((e) => e.text.includes('data'))


// ---- Deleting an object
await dbInterface.del(obj._id)
```
